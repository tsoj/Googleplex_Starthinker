#cutechess-cli
#-concurrency 24
#-games 2
#-rounds 400
#-openings file=./Blitz_Testing_4moves.pgn order=random
#-repeat
#-each tc=40/60+0 proto=uci dir=../
#-engine name=Googleplex_Starthinker_1.4 cmd=Googleplex_Starthinker-version-1.4
#-engine name=Googleplex_Starthinker_1.6beta cmd=Googleplex_Starthinker-version-1.6beta

../cutechess/projects/cli/cutechess-cli -recover -debugoncrash -concurrency 45 -games 2 -rounds 500 -openings file=./Blitz_Testing_4moves.pgn order=random -repeat 2 -each tc=40/60+0 proto=uci dir=./ -engine name=Googleplex_Starthinker_old cmd=./Googleplex_Starthinker_old -engine name=Googleplex_Starthinker_new cmd=./Googleplex_Starthinker_new
