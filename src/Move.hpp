#pragma once

#include "types.hpp"
#include "util.hpp"

struct Move
{
    uint32_t m_data;
    /*
    from: [0...6], to: [7...13], moved: [14...16], captured: [17...19], promoted: [20...22],
    castled: 23, captured_enpassant: 24, enPassant square: [25..31]
    */
public:

    inline constexpr void set(
        const Square& from,
        const Square& to,
        const Piece& moved,
        const Piece& captured,
        const Piece& promoted,
        const bool& castled,
        const bool& captured_enpassant,
        const Square& enpassant
    )
    {
        m_data = 0;
        m_data |= (from & 0b1111111);
        m_data |= ((to & 0b1111111) << 7);
        m_data |= ((moved & 0b111) << 14);
        m_data |= ((captured & 0b111) << 17);
        m_data |= ((promoted & 0b111) << 20);
        m_data |= ((castled & 0b1) << 23);
        m_data |= ((captured_enpassant & 0b1) << 24);
        m_data |= ((enpassant & 0b1111111) << 25);
    }

    [[nodiscard]] inline constexpr Square from() const
    { return (m_data & 0b1111111); }

    [[nodiscard]] inline constexpr Square to() const
    { return ((m_data >> 7) & 0b1111111); }

    [[nodiscard]] inline constexpr Piece moved() const
    { return ((m_data >> 14) & 0b111); }

    [[nodiscard]] inline constexpr Piece captured() const
    { return ((m_data >> 17) & 0b111); }

    [[nodiscard]] inline constexpr Piece promoted() const
    { return ((m_data >> 20) & 0b111); }

    [[nodiscard]] inline constexpr bool castled() const
    { return ((m_data >> 23) & 0b1); }

    [[nodiscard]] inline constexpr bool captured_enpassant() const
    { return ((m_data >> 24) & 0b1); }

    [[nodiscard]] inline constexpr Square enpassant() const
    { return ((m_data >> 25) & 0b1111111); }

    inline bool operator==(const Move& m) const
    { return this->m_data == m.m_data; }

    inline bool operator!=(const Move& m) const
    { return this->m_data != m.m_data; }

    [[nodiscard]] std::string getString() const
    {
        std::string ret;
        ret += "from: " + util::getSquareNotation(from()) + "\n";
        ret += "to: " + util::getSquareNotation(to()) + "\n";
        ret += "moved: " + util::getPieceNotation(white, moved()) + "\n";
        ret += "captured: " + util::getPieceNotation(white, captured()) + "\n";
        ret += "promoted: " + util::getPieceNotation(white, promoted()) + "\n";
        ret += "castled: " + std::to_string(castled()) + "\n";
        ret += "captured_enpassant: " + std::to_string(captured_enpassant()) + "\n";
        ret += "enpassant: " + util::getSquareNotation(enpassant()) + "\n";
        return ret;
    }

    [[nodiscard]] std::string getNotation() const
    {
        std::string ret = "";
        ret += util::getSquareNotation(from());
        ret += util::getSquareNotation(to());
        if (promoted() != no_piece)
        {
            ret += util::getPieceNotation(black, promoted());
        }
        return ret;
    }

    [[nodiscard]] Score mvvLva() const
    {
        Score score = 0;
        score += score_list[captured()];
        if (promoted() != no_piece)
        {
            score += score_list[promoted()] - score_pawn;
        }
        score -= score_list[moved()] / 10;
        return score;
    }

    [[nodiscard]] bool isCapture() const
    {
        if (captured() != no_piece)
        {
            return true;
        }
        return false;
    }

    [[nodiscard]] bool isTactical() const
    {
        return
            isCapture() ||
            promoted() != no_piece;
    }
};

constexpr Move no_move{2154668096};
static_assert(no_move.from() == no_square);
static_assert(no_move.to() == no_square);
static_assert(no_move.moved() == no_piece);
static_assert(no_move.captured() == no_piece);
static_assert(no_move.promoted() == no_piece);
static_assert(no_move.castled() == false);
static_assert(no_move.captured_enpassant() == false);
static_assert(no_move.enpassant() == no_square);
