#pragma once

#include "types.hpp"
#include "Move.hpp"
#include "Position.hpp"

#include <vector>

class HashTable
{
    struct Entry
    {
        uint64_t zobrist_key;
        NodeType node_type;
        Score score;
        Ply depth;
        Move best_move;
        int8_t lookup_counter;

        [[nodiscard]] bool isEmpty() const
        {
            return zobrist_key == 0;
        }
    };

public:

    explicit HashTable(size_t size);

    void clear();

    void age();

    [[nodiscard]] int hashFull() const;

    void add(const uint64_t zobrist_key, const NodeType node_type, const Score score, const Ply depth, const Move best_move);

    [[nodiscard]] const Entry& get(const uint64_t zobrist_key);

    void prefetch(const uint64_t zobrist_key) const;

    [[nodiscard]] std::vector<Move> getPv(Position position) const;

private:

    inline constexpr static Entry no_entry = Entry{0, no_node, no_score, 0, no_move, 0};

    std::vector<Entry> always_replace;
    std::vector<Entry> selective_replace;
};
