#include "Position.hpp"
#include "bitboard.hpp"
#include "castling.hpp"
#include "movegen.hpp"
#include "infoassert.hpp"
#include "util.hpp"
#include <iostream>

namespace movegen
{
    uint8_t generateCaptures(const Piece piece, Move* moves, const Position& position)
    {
        uint8_t counter = 0;

        const Player us = position.us;
        const Player enemy = position.enemy;

        uint64_t pieces = position.pieces[piece] & position.players[us];
        const uint64_t occupancy = position.players[white] | position.players[black];

        while (pieces != 0)
        {
            const Square from = bitboard::rto(pieces);
            uint64_t attack_mask = bitboard::attackMask(piece, from, occupancy) & position.players[enemy];

            while (attack_mask != 0)
            {
                const Square to = bitboard::rto(attack_mask);

                for (Piece captured = 0; captured < no_piece; captured += 1)
                {
                    if ((position.pieces[captured] & bitboard::bitAtIndex(to)) != 0)
                    {
                        moves[counter].set(
                            from,
                            to,
                            piece,
                            captured,
                            no_piece,
                            false,
                            false,
                            no_square
                        );
                        counter += 1;
                        break;
                    }
                }
            }
        }
        return counter;
    }

    uint8_t generateQuiets(const Piece piece, Move* moves, const Position& position)
    {
        uint8_t counter = 0;

        const Player us = position.us;

        uint64_t pieces = position.pieces[piece] & position.players[us];
        const uint64_t occupancy = position.players[white] | position.players[black];

        while (pieces != 0)
        {
            const Square from = bitboard::rto(pieces);
            uint64_t attack_mask = bitboard::attackMask(piece, from, occupancy) & ~occupancy;

            while (attack_mask != 0)
            {
                const Square to = bitboard::rto(attack_mask);

                moves[counter].set(
                    from,
                    to,
                    piece,
                    no_piece,
                    no_piece,
                    false,
                    false,
                    no_square
                );
                counter += 1;
            }
        }
        return counter;

    }

    uint8_t generatePawnCaptures(Move* moves, const Position& position)
    {
        uint8_t counter = 0;

        const Player us = position.us;
        const Player enemy = position.enemy;

        uint64_t pawns = position.pieces[pawn] & position.players[us];
        uint64_t occupancy = position.players[white] | position.players[black];

        while (pawns != 0)
        {
            const Square from = bitboard::rto(pawns);

            if (
                (bitboard::bitAtIndex(from) & bitboard::pawnHomeRank(enemy)) != 0 &&
                    (bitboard::attackMaskPawnQuiet(us, from) & occupancy) == 0
                )
            {
                const Square to = bitboard::ctz(bitboard::attackMaskPawnQuiet(us, from));
                moves[counter].set(from, to, pawn, no_piece, knight, false, false, no_square);
                counter += 1;
                moves[counter].set(from, to, pawn, no_piece, bishop, false, false, no_square);
                counter += 1;
                moves[counter].set(from, to, pawn, no_piece, rook, false, false, no_square);
                counter += 1;
                moves[counter].set(from, to, pawn, no_piece, queen, false, false, no_square);
                counter += 1;
            }

            uint64_t attack_mask = bitboard::attackMaskPawnCapture(us, from) & position.players[enemy];
            while (attack_mask != 0)
            {
                const Square to = bitboard::rto(attack_mask);
                for (Piece captured = 0; captured < no_piece; captured += 1)
                {
                    if ((position.pieces[captured] & bitboard::bitAtIndex(to)) != 0)
                    {
                        if ((bitboard::bitAtIndex(to) & bitboard::homeRank(enemy)) != 0)
                        {
                            moves[counter].set(from, to, pawn, captured, knight, false, false, no_square);
                            counter += 1;
                            moves[counter].set(from, to, pawn, captured, bishop, false, false, no_square);
                            counter += 1;
                            moves[counter].set(from, to, pawn, captured, rook, false, false, no_square);
                            counter += 1;
                            moves[counter].set(from, to, pawn, captured, queen, false, false, no_square);
                            counter += 1;
                        }
                        else
                        {
                            moves[counter].set(from, to, pawn, captured, no_piece, false, false, no_square);
                            counter += 1;
                        }
                        break;
                    }
                }
            }

            //enPassant capture
            attack_mask = bitboard::attackMaskPawnCapture(us, from) & position.enpassant_castling & (bitboard::rank(a3) | bitboard::rank(a6));
            if (attack_mask != 0)
            {
                const Square to = bitboard::ctz(attack_mask);
                moves[counter].set(from, to, pawn, pawn, no_piece, false, true, no_square);
                counter += 1;
            }

        }
        return counter;
    }

    uint8_t generatePawnQuiets(Move* moves, const Position& position)
    {
        uint8_t counter = 0;

        const Player us = position.us;
        const Player enemy = position.enemy;

        uint64_t pawns = position.pieces[pawn] & position.players[us];
        uint64_t occupancy = position.players[white] | position.players[black];

        while (pawns != 0)
        {
            const Square from = bitboard::rto(pawns);

            if ((bitboard::attackMaskPawnQuiet(us, from) & (occupancy | bitboard::homeRank(enemy))) == 0)
            {
                const Square to = bitboard::ctz(bitboard::attackMaskPawnQuiet(us, from));

                moves[counter].set(from, to, pawn, no_piece, no_piece, false, false, no_square);
                counter += 1;

                if ((bitboard::bitAtIndex(from) & bitboard::pawnHomeRank(us)) != 0)
                {
                    const Square doublePushTo = bitboard::ctz(bitboard::attackMaskPawnQuiet(us, to));

                    if ((bitboard::bitAtIndex(doublePushTo) & occupancy) == 0)
                    {
                        moves[counter].set(from, doublePushTo, pawn, no_piece, no_piece, false, false, to);
                        counter += 1;
                    }
                }
            }
        }
        return counter;
    }

    uint8_t generateCastlingMoves(Move* moves, const Position& position)
    {
        uint8_t counter = 0;

        const Player enemy = position.enemy;
        const Player us = position.us;

        if ((position.enpassant_castling & castling::king_from[us]) != 0)
        {
            const uint64_t occupancy = position.players[white] | position.players[black];
            // queenside castling
            if (
                (position.enpassant_castling & castling::queenside_rook_from[us]) != 0 &&
                (castling::queenside_block_relevant_area[us] & occupancy) == 0 &&
                !position.attacked(us, enemy, castling::queenside_check_relevant_squares[us][0]) &&
                !position.attacked(us, enemy, castling::queenside_check_relevant_squares[us][1])
                )
            {
                moves[counter].set(
                    castling::king_from_square[us],
                    castling::queenside_king_to_square[us],
                    king,
                    no_piece,
                    no_piece,
                    true,
                    false,
                    no_square
                );
                counter += 1;
            }
            // kingside castling
            if (
                (position.enpassant_castling & castling::kingside_rook_from[us]) != 0 &&
                (castling::kingside_block_relevant_area[us] & occupancy) == 0 &&
                !position.attacked(us, enemy, castling::kingside_check_relevant_squares[us][0]) &&
                !position.attacked(us, enemy, castling::kingside_check_relevant_squares[us][1])
                )
            {
                moves[counter].set(
                    castling::king_from_square[us],
                    castling::kingside_king_to_square[us],
                    king,
                    no_piece,
                    no_piece,
                    true,
                    false,
                    no_square
                );
                counter += 1;
            }
        }
        return counter;
    }
}
