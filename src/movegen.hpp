#pragma once

#include "types.hpp"
#include "Position.hpp"

namespace movegen
{
    [[nodiscard]] uint8_t generateCaptures(const Piece piece, Move* moves, const Position& position);

    [[nodiscard]] uint8_t generateQuiets(const Piece piece, Move* moves, const Position& position);

    [[nodiscard]] uint8_t generatePawnCaptures(Move* moves, const Position& position);

    [[nodiscard]] uint8_t generatePawnQuiets(Move* moves, const Position& position);

    [[nodiscard]] uint8_t generateCastlingMoves(Move* moves, const Position& position);
}
