#include "perft.hpp"
#include "uci.hpp"
#include "util.hpp"

#include <string>

int main(int argc, char** argv)
{
    for (int i = 0; i < argc; i++)
    {
        if (std::string(argv[i]) == "--perft" || std::string(argv[i]) == "-p")
        {
            perft::test();
        }
    }
    uci::uciLoop();
    return 0;
}
