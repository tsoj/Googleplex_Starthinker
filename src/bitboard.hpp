#pragma once

#include "types.hpp"

#include <cassert>
#include <array>
#include <algorithm>

namespace bitboard
{
#if defined(__GNUC__) || defined(__clang__)

    inline constexpr int ctz(const uint64_t word)
    {
        if (word == 0)
        {
            return 64;
        }
        return __builtin_ctzll(word);
    }

    inline constexpr int clz(const uint64_t word)
    {
        if (word == 0)
        {
            return 64;
        }
        return __builtin_clzll(word);
    }

    inline constexpr int popcount(const uint64_t word)
    {
        return __builtin_popcountll(word);
    }

#elif defined(_MSC_VER)
#include <intrin.h>
    inline constexpr int popcount(const uint64_t word)
    {
        return __popcnt64(word);
    }
    inline constexpr int ctz(const uint64_t word)
    {
        unsigned long ret;
        _BitScanForward64(&ret, word);
        return ret;
    }
    inline constexpr int clz(const uint64_t word)
    {
        return __lzcnt64(word);
    }
#else
    inline constexpr int popcount(const uint64_t word)
    {
        int counter = 0;
        for (int i = 0; i < 64; i++)
        {
            if ((word & ((uint64_t) 0b1U) << i) != 0)
            {
                counter++;
            }
        }
        return counter;
    }
    inline constexpr int ctz(const uint64_t word)
    {
        for (int i = 0; i < 64; i++)
        {
            if ((word & ((uint64_t) 0b1U) << i) != 0)
            {
                return i;
            }
        }
        return 64;
    }
    inline constexpr int clz(const uint64_t word)
    {
        for (int i = 63; i >= 0; i--)
        {
            if ((word & ((uint64_t) 0b1U) >> i) != 0)
            {
                return i;
            }
        }
        return 64;
    }
#endif

    inline constexpr int rto(uint64_t& word)
    {
        int ret = ctz(word);
        word &= word - 1;
        return ret;
    }

#include "bitboard_details.inl"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

    inline constexpr uint64_t attackMaskKnight(const Square square, const uint64_t occupancy)
    {
        return details::knight_attack_table[square];
    }

    inline constexpr uint64_t attackMaskBishop(const Square square, const uint64_t occupancy)
    {
        return details::anti_diagonal_attack_table[square][details::hashkeyAntiDiagonal(square, occupancy)] |
               details::diagonal_attack_table[square][details::hashkeyDiagonal(square, occupancy)];
    }

    inline constexpr uint64_t attackMaskRook(const Square square, const uint64_t occupancy)
    {
        return details::rank_attack_table[square][details::hashkeyRank(square, occupancy)] |
               details::file_attack_table[square][details::hashkeyFile(square, occupancy)];
    }

    inline constexpr uint64_t attackMaskQueen(const Square square, const uint64_t occupancy)
    {
        return details::anti_diagonal_attack_table[square][details::hashkeyAntiDiagonal(square, occupancy)] |
               details::diagonal_attack_table[square][details::hashkeyDiagonal(square, occupancy)] |
               details::rank_attack_table[square][details::hashkeyRank(square, occupancy)] |
               details::file_attack_table[square][details::hashkeyFile(square, occupancy)];
    }

    inline constexpr uint64_t attackMaskKing(const Square square, const uint64_t occupancy)
    {
        return details::king_attack_table[square];
    }

#pragma GCC diagnostic pop

    inline constexpr uint64_t attackMask(const Piece piece, const Square square, const uint64_t occupancy)
    {
        constexpr uint64_t (* attack_mask_Functions[])(const Square, const uint64_t) =
            {
                nullptr, // pawn
                attackMaskKnight, // knight
                attackMaskBishop, // bishop
                attackMaskRook, // rook
                attackMaskQueen, // queen
                attackMaskKing, // king
            };
        assert(piece != pawn);
        return attack_mask_Functions[piece](square, occupancy);
    }

    inline constexpr uint64_t attackMaskPawnQuiet(const Player player, const Square square)
    {
        return details::pawn_quiet_attack_table[player][square];
    }

    inline constexpr uint64_t attackMaskPawnCapture(const Player player, const Square square)
    {
        return details::pawn_capture_attack_table[player][square];
    }

    inline constexpr uint64_t bitAtIndex(const Square square)
    {
        return details::bit_at_index[square];
    }

    inline constexpr uint64_t file(const Square square)
    {
        return details::files_64[square];
    }

    inline constexpr uint64_t rank(const Square square)
    {
        return details::ranks_64[square];
    }

    inline constexpr uint64_t diagonal(const Square square)
    {
        return details::diagonals_64[square];
    }

    inline constexpr uint64_t antiDiagonal(const Square square)
    {
        return details::anti_diagonals_64[square];
    }

    inline constexpr uint64_t zobristMaskPieces(const Piece piece, const Square square)
    {
        return details::zobrist_random_bitmasks_pieces[piece][square];
    }

    inline constexpr uint64_t zobristMaskPlayers(const Player player, const Square square)
    {
        return details::zobrist_random_bitmasks_players[player][square];
    }

    inline constexpr uint64_t zobristMaskSideToMove(const Player player)
    {
        return details::zobrist_random_bitmasks_side_to_move[player];
    }

    inline constexpr uint64_t isPassed(const Player player, const Square square)
    {
        return details::is_passed[player][square];
    }

    inline constexpr uint64_t homeRank(const Player player)
    {
        return details::home_rank[player];
    }

    inline constexpr uint64_t pawnHomeRank(const Player player)
    {
        return details::pawn_home_rank[player];
    }

    inline constexpr uint64_t getMask(const int8_t size, const Square square)
    {
        return details::nxn_mask[std::min<int8_t>(size, 5)][square];
    }

}
