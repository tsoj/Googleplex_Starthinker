#pragma once

#include "Move.hpp"
#include "HashTable.hpp"

#include <vector>
#include <atomic>
#include <memory>

namespace search
{
    std::pair<Move, Score> go(
        const Position& position,
        HashTable& hash_table,
        const std::vector<Position>& game_history,
        const Ply target_depth,
        const int ms_left,
        const int ms_inc_per_move,
        const int moves_to_go,
        std::atomic<bool>& stop,
        std::ostream& output
    );
}
