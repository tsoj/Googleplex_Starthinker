#include "HashTable.hpp"

#include <iostream>

namespace
{
    size_t calculateIndex(const uint64_t zobrist_key, const size_t size)
    {
        return zobrist_key % size;
    }
}

HashTable::HashTable(size_t size)
{
    size = size / sizeof(HashTable::Entry);
    constexpr float always_replace_factor = 0.95;
    constexpr float selective_replace_factor = 0.05;
    static_assert(always_replace_factor + selective_replace_factor == 1.0);
    always_replace = std::vector<HashTable::Entry>(size * always_replace_factor);
    selective_replace = std::vector<HashTable::Entry>(size * selective_replace_factor);
    clear();
}

void HashTable::clear()
{
    for (size_t i = 0; i < always_replace.size(); i++)
    {
        always_replace[i] = no_entry;
    }
    for (size_t i = 0; i < selective_replace.size(); i++)
    {
        selective_replace[i] = no_entry;
    }
}

void HashTable::age()
{
    for (size_t i = 0; i < selective_replace.size(); i++)
    {
        if (!selective_replace[i].isEmpty())
        {
            if (selective_replace[i].lookup_counter <= 0)
            {
                selective_replace[i] = no_entry;
            }
            else
            {
                selective_replace[i].lookup_counter = 0;
            }
        }

    }
}

int HashTable::hashFull() const
{
    /*TODO is to slow
    size_t counter = 1;
    for (const auto& entry : always_replace)
    {
        if (!entry.is_empty())
        {
            counter += 1;
        }
    }
    for (const auto& entry : selective_replace)
    {
        if (!entry.is_empty())
        {
            counter += 1;
        }
    }
    return counter * 1000 / (selective_replace.size() + always_replace.size());*/
    return 0;
}

void HashTable::add(const uint64_t zobrist_key, const NodeType node_type, const Score score, const Ply depth, const Move best_move)
{
    // always replace
    size_t index = calculateIndex(zobrist_key, always_replace.size());
    always_replace[index] = Entry{zobrist_key, node_type, score, depth, best_move, 0};

    // selective replace
    index = calculateIndex(zobrist_key, selective_replace.size());

    if (!selective_replace[index].isEmpty())
    {
        switch (node_type)
        {
            case pv_node:
                if (selective_replace[index].node_type == pv_node && selective_replace[index].depth > depth)
                {
                    return;
                }
                break;
            case cut_node:
            case all_node:
                if (selective_replace[index].node_type == pv_node || selective_replace[index].depth > depth)
                {
                    return;
                }
                break;
            default:
                infoassert(false);
        }
    }
    selective_replace[index] = Entry{zobrist_key, node_type, score, depth, best_move, 1};
}

const HashTable::Entry& HashTable::get(const uint64_t zobrist_key)
{
    size_t index = calculateIndex(zobrist_key, selective_replace.size());
    if (zobrist_key == selective_replace[index].zobrist_key)
    {
        selective_replace[index].lookup_counter += 1;
        if (selective_replace[index].lookup_counter < 0)
        {
            selective_replace[index].lookup_counter = INT8_MAX;
        }
        return selective_replace[index];
    }

    index = calculateIndex(zobrist_key, always_replace.size());
    if (zobrist_key == always_replace[index].zobrist_key)
    {
        return always_replace[index];
    }

    return no_entry;
}

void HashTable::prefetch(const uint64_t zobrist_key) const
{
    size_t index = calculateIndex(zobrist_key, always_replace.size());
    __builtin_prefetch(&always_replace[index]);
    index = calculateIndex(zobrist_key, selective_replace.size());
    __builtin_prefetch(&selective_replace[index]);
}

std::vector<Move> HashTable::getPv(Position position) const
{
    std::vector<Move> pv = std::vector<Move>();
    std::vector<uint64_t> done = std::vector<uint64_t>();
    while (true)
    {
        for (auto z : done)
        {
            if (z == position.zobrist_key)
            {
                return pv;
            }
        }
        done.push_back(position.zobrist_key);
        Move move = no_move;

        size_t index = calculateIndex(position.zobrist_key, selective_replace.size());
        if (position.zobrist_key == selective_replace[index].zobrist_key)
        {
            move = selective_replace[index].best_move;
        }
        else
        {
            index = calculateIndex(position.zobrist_key, always_replace.size());
            if (position.zobrist_key == always_replace[index].zobrist_key)
            {
                move = always_replace[index].best_move;
            }
        }

        if (move == no_move)
        {
            return pv;
        }
        pv.push_back(move);
        position.doMove(move);
    }
}
