#pragma once

#include "types.hpp"
#include "bitboard.hpp"

using namespace bitboard;

namespace castling
{
    constexpr Square queenside_rook_from_square[2] = {a1, a8};
    constexpr uint64_t queenside_rook_from[2] = {bitAtIndex(queenside_rook_from_square[0]), bitAtIndex(queenside_rook_from_square[1])};
    constexpr Square kingside_rook_from_square[2] = {h1, h8};
    constexpr uint64_t kingside_rook_from[2] = {bitAtIndex(kingside_rook_from_square[0]), bitAtIndex(kingside_rook_from_square[1])};
    constexpr Square queenside_rook_to_square[2] = {d1, d8};
    constexpr uint64_t queenside_rook_to[2] = {bitAtIndex(queenside_rook_to_square[0]), bitAtIndex(queenside_rook_to_square[1])};
    constexpr Square kingside_rook_to_square[2] = {f1, f8};
    constexpr uint64_t kingside_rook_to[2] = {bitAtIndex(kingside_rook_to_square[0]), bitAtIndex(kingside_rook_to_square[1])};
    constexpr Square king_from_square[2] = {e1, e8};
    constexpr uint64_t king_from[2] = {bitAtIndex(king_from_square[0]), bitAtIndex(king_from_square[1])};
    constexpr Square queenside_king_to_square[2] = {c1, c8};
    constexpr uint64_t queenside_king_to[2] = {bitAtIndex(queenside_king_to_square[0]), bitAtIndex(queenside_king_to_square[1])};
    constexpr Square kingside_king_to_square[2] = {g1, g8};
    constexpr uint64_t kingside_king_to[2] = {bitAtIndex(kingside_king_to_square[0]), bitAtIndex(kingside_king_to_square[1])};
    constexpr Square queenside_check_relevant_squares[2][2] = {{d1, e1},
                                                               {d8, e8}};
    constexpr Square kingside_check_relevant_squares[2][2] = {{e1, f1},
                                                              {e8, f8}};
    constexpr uint64_t queenside_block_relevant_area[2] =
        {bitAtIndex(b1) | bitAtIndex(c1) | bitAtIndex(d1), bitAtIndex(b8) | bitAtIndex(c8) | bitAtIndex(d8)};
    constexpr uint64_t kingside_block_relevant_area[2] = {bitAtIndex(f1) | bitAtIndex(g1), bitAtIndex(f8) | bitAtIndex(g8)};
}
