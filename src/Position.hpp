#pragma once

#include <string>

#include "types.hpp"
#include "Move.hpp"
#include "bitboard.hpp"

struct Position
{
    uint64_t pieces[6]; // [Pawns, Knights, Bishops, Rooks, Queens, Kings]
    uint64_t players[2]; // [White pieces, Black pieces]
    uint64_t enpassant_castling;
    uint64_t zobrist_key;
    Player us;
    Player enemy;
    int16_t fullmoves_played;
    int16_t halfmove_clock;

    static Position fen(const std::string& fen);

    [[nodiscard]] std::string getFen() const;

    [[nodiscard]] std::string getString() const;

    [[nodiscard]] std::string debugString() const;

    [[nodiscard]] std::string peudoLegalMoves() const;

    [[nodiscard]] std::pair<Piece, Player> getPiece(const Square square) const;

    void addPiece(const Player player, const Piece piece, const uint64_t to);

    void removePiece(const Player player, const Piece piece, const uint64_t from);

    void movePiece(const Player player, const Piece piece, const uint64_t from, const uint64_t to);

    [[nodiscard]] uint64_t occupancy() const;

    [[nodiscard]] uint64_t attackers(const Player us, const Player enemy, const Square attacked_square) const;

    [[nodiscard]] bool attacked(const Player us, const Player enemy, const Square attacked_square) const;

    [[nodiscard]] bool inCheck(const Player us, const Player enemy) const;

    [[nodiscard]] bool isPassedPawn(const Player us, const Player enemy, const Square square) const;

    [[nodiscard]] bool isPseudoLegal(const Move move) const;

    [[nodiscard]] bool isLegal(const Move move) const;

    [[nodiscard]] bool insufficientMaterial() const;

    [[nodiscard]] Phase getPhase() const;

    [[nodiscard]] Score evaluate() const;

    [[nodiscard]] Score see(const Square attacked_square, Piece attacked_piece) const;

    [[nodiscard]] Score see(const Move move) const;

    [[nodiscard]] uint64_t getZobristKey() const;

    uint8_t generateCaptures(Move* moves) const;

    uint8_t generateQuiets(Move* moves) const;

    uint8_t generateMoves(Move* moves) const;

    void doMove(const Move move);

    void doNullMove();
};
