#pragma once

#include "types.hpp"
#include "infoassert.hpp"

#include <string>
#include <vector>
#include <atomic>
#include <cmath>

struct Move;
struct Position;

namespace util
{
    std::string getUnicodePiece(const Player& player, const Piece& piece);

    std::string getPieceNotation(const Player& player, const Piece& piece);

    constexpr Square mirror(const Square& s)
    {
        return Square((7 - s / 8) * 8 + s % 8);
    }

    Square rotateCounterclockwise(const Square& s);

    std::string getSquareNotation(const Square& i);

    Square getSquareIndex(const std::string& s);

    bool isMove(const std::string move_string);

    Move getMove(const std::string move_string, const Position position);

    std::string getString(const uint64_t& word);

    std::string getString(std::vector<Move> moves);

    void setIn(long milliseconds, std::atomic<bool>& set);

    std::vector<std::string> split(const std::string& s, char delim);

    EndOfGame gameEnded(const Position& position, const std::vector<Position>& history);

    inline Player getSquareColor(const Square square)
    {
        if ((square / 8) % 2 == 0 && (square % 8) % 2 == 0)
        {
            return black;
        }
        if ((square / 8) % 2 == 1 && (square % 8) % 2 == 1)
        {
            return black;
        }
        return white;
    }

    inline auto getPiece(const std::string& s)
    {
        struct Ret
        {
            Player player;
            Piece piece;
        };
        if (s == "p")
        {
            return Ret{black, pawn};
        }
        if (s == "n")
        {
            return Ret{black, knight};
        }
        if (s == "b")
        {
            return Ret{black, bishop};
        }
        if (s == "r")
        {
            return Ret{black, rook};
        }
        if (s == "q")
        {
            return Ret{black, queen};
        }
        if (s == "k")
        {
            return Ret{black, king};
        }

        if (s == "P")
        {
            return Ret{white, pawn};
        }
        if (s == "N")
        {
            return Ret{white, knight};
        }
        if (s == "B")
        {
            return Ret{white, bishop};
        }
        if (s == "R")
        {
            return Ret{white, rook};
        }
        if (s == "Q")
        {
            return Ret{white, queen};
        }
        if (s == "K")
        {
            return Ret{white, king};
        }
        return Ret{no_player, no_piece};
    }
}
