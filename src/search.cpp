#include "Position.hpp"
#include "bitboard.hpp"
#include "util.hpp"
#include "search.hpp"
#include "perft.hpp"
#include "infoassert.hpp"

#include <algorithm>
#include <chrono>
#include <array>
#include <memory>
#include <functional>

#include <thread>

#include <iostream>
#include <utility>

namespace search
{
    namespace
    {
        class HistoryTable
        {
        private:

            std::array<Score, 6 * 64 * 2> piece_to_square{};

            void scale_down()
            {
                for (auto& score : piece_to_square)
                {
                    score = score / 2;
                }
            }

            static size_t get_index(const Move move, const Player player)
            {
                infoassert(player != no_player);
                infoassert(move.to() != no_square);
                infoassert(move.moved() != no_piece);
                return player + 2 * move.to() + 2 * 64 * move.moved();
            }

            constexpr static Score max_value = score_of_all_pieces;

        public:

            HistoryTable()
            {
                std::fill(piece_to_square.begin(), piece_to_square.end(), 0);
            }

            void update(const Move move, const Player player, const Ply depth)
            {
                if (!move.isTactical())
                {
                    piece_to_square[get_index(move, player)] += depth * depth;
                    if (piece_to_square[get_index(move, player)] > max_value)
                    {
                        scale_down();
                    }
                }
            }

            [[nodiscard]] const Score& get_score(const Move move, const Player player) const
            {
                return piece_to_square[get_index(move, player)];
            }
        };

        class KillerTable
        {
        private:

            std::array<std::array<Move, 2>, max_depth> killer;

        public:
            KillerTable()
            {
                std::fill_n(&killer[0][0], sizeof(killer) / sizeof(killer[0][0]), no_move);
            }

            void update(const Ply height, const Move move)
            {
                if (!move.isTactical())
                {
                    killer[height][1] = killer[height][0];
                    killer[height][0] = move;
                }
            }

            const std::array<Move, 2>& get(const Ply height)
            {
                infoassert(height < max_depth);
                return killer[height];
            }
        };

        class MoveSelector
        {
        private:

            enum class Stage
            {
                hash_move,
                init_captures,
                winning_captures,
                loosing_captures,
                killer_0,
                killer_1,
                init_quiets,
                quiets,
                done
            };
            Stage m_stage;
            Move m_moves[std::max(max_num_captures, max_num_quiets)];
            int32_t m_move_priority[std::max(max_num_captures, max_num_quiets)];
            uint8_t m_num_moves;
            Position m_position;
            Move m_hash_move;
            std::array<Move, 2> m_killer;
            std::function<size_t(const Move, const Player)> m_get_history_score;

        public:

            void init(
                const Position& position, const Move hash_move, const std::array<Move, 2>& killer,
                std::function<size_t(const Move, const Player)> get_history_score
            )
            {
                m_stage = Stage::hash_move;
                this->m_position = position;
                this->m_hash_move = hash_move;
                this->m_killer = killer;
                this->m_get_history_score = std::move(get_history_score);
            }

            void init(const Position& position, const Move hash_move = no_move)
            {
                m_stage = Stage::hash_move;
                this->m_position = position;
                m_hash_move = hash_move;
                std::fill(m_killer.begin(), m_killer.end(), no_move);
                m_get_history_score = [](const Move, const Player)
                { return 0; };
            }

            bool next(Move& move, bool no_quiets = false)
            {
                while (true)
                {
                    switch (m_stage)
                    {
                        case Stage::hash_move:
                        {
                            m_stage = Stage::init_captures;
                            if (m_hash_move == no_move || !m_position.isPseudoLegal(m_hash_move))
                            {
                                continue;
                            }
                            else
                            {
                                move = m_hash_move;
                                return true;
                            }
                        }
                        case Stage::init_captures:
                        {
                            m_num_moves = m_position.generateCaptures(m_moves);
                            for (uint8_t i = 0; i < m_num_moves; i++)
                            {
                                m_move_priority[i] = m_position.see(m_moves[i]);
                            }
                            m_stage = Stage::winning_captures;
                            continue;
                        }
                        case Stage::winning_captures:
                        {
                            int32_t max_priority = 0;
                            uint8_t best = m_num_moves;

                            for (uint8_t i = 0; i < m_num_moves; i++)
                            {
                                if (m_move_priority[i] > max_priority && m_moves[i] != m_hash_move)
                                {
                                    best = i;
                                    max_priority = m_move_priority[i];
                                }
                            }
                            if (best < m_num_moves)
                            {
                                move = m_moves[best];
                                m_move_priority[best] = -score_infinity;
                            }
                            else
                            {
                                m_stage = Stage::killer_0;
                                if (no_quiets)
                                {
                                    m_stage = Stage::loosing_captures;
                                }
                                continue;
                            }
                            return true;
                        }
                        case Stage::killer_0:
                        {
                            m_stage = Stage::killer_1;
                            if (
                                m_killer[0] == no_move ||
                                m_killer[0] == m_hash_move ||
                                !m_position.isPseudoLegal(m_killer[0])
                                )
                            {
                                continue;
                            }
                            move = m_killer[0];
                            return true;
                        }
                        case Stage::killer_1:
                        {
                            m_stage = Stage::loosing_captures;
                            if (
                                m_killer[1] == no_move ||
                                !m_position.isPseudoLegal(m_killer[1]) ||
                                m_killer[1] == m_hash_move ||
                                m_killer[1] == m_killer[0]
                                )
                            {
                                continue;
                            }
                            move = m_killer[1];
                            return true;
                        }
                        case Stage::loosing_captures:
                        {
                            int32_t max_priority = -score_infinity;
                            uint8_t best = m_num_moves;

                            for (uint8_t i = 0; i < m_num_moves; i++)
                            {
                                if (m_move_priority[i] > max_priority && m_moves[i] != m_hash_move)
                                {
                                    best = i;
                                    max_priority = m_move_priority[i];
                                }
                            }
                            if (best < m_num_moves)
                            {
                                move = m_moves[best];
                                m_move_priority[best] = -score_infinity;
                            }
                            else
                            {
                                m_stage = Stage::init_quiets;
                                if (no_quiets)
                                {
                                    m_stage = Stage::done;
                                }
                                continue;
                            }
                            return true;
                        }
                        case Stage::init_quiets:
                        {
                            m_num_moves = m_position.generateQuiets(m_moves);
                            for (uint8_t i = 0; i < m_num_moves; i++)
                            {
                                // TODO: relative history heuristic, piece square tables, guard heuristic, (?some kind of neural net?)
                                m_move_priority[i] = m_get_history_score(m_moves[i], m_position.us);
                            }
                            m_stage = Stage::quiets;
                            continue;
                        }
                        case Stage::quiets:
                        {
                            int32_t max_priority = -score_infinity;
                            uint8_t best = m_num_moves;

                            for (uint8_t i = 0; i < m_num_moves; i++)
                            {
                                if (
                                    m_move_priority[i] > max_priority &&
                                    m_moves[i] != m_hash_move &&
                                    m_moves[i] != m_killer[0] &&
                                    m_moves[i] != m_killer[1]
                                    )
                                {
                                    best = i;
                                    max_priority = m_move_priority[i];
                                }
                            }
                            if (best < m_num_moves)
                            {
                                move = m_moves[best];
                                m_move_priority[best] = -score_infinity;
                            }
                            else
                            {
                                m_stage = Stage::done;
                                return false;
                            }
                            return true;
                        }
                        default:
                        {
                            return false;
                        }
                    }
                }
            }
        };

        class GameHistory
        {
            uint64_t m_history[max_depth];
            std::vector<uint64_t> m_static_history;
        public:

            GameHistory(const std::vector<Position>& history)
            {
                m_static_history = std::vector<uint64_t>();
                for (size_t i = 0; i + 1 < history.size(); i++)
                {
                    const auto& position = history[i];
                    m_static_history.push_back(position.zobrist_key);
                }
            }

            void update(uint64_t zobrist_key, Ply height)
            {
                m_history[height] = zobrist_key;
            }

            bool checkForRepetition(uint64_t zobrist_key, Ply height)
            {
                for (Ply i = 0; i < height; i++)
                {
                    if (zobrist_key == m_history[i])
                    {
                        return true;
                    }
                }
                for (uint64_t i : m_static_history)
                {
                    if (zobrist_key == i)
                    {
                        return true;
                    }
                }
                return false;
            }
        };

        struct SearchStruct
        {
            HashTable& hash_table;
            KillerTable killer;
            HistoryTable history;
            GameHistory game_history;
            const std::atomic<bool>& stop;
            uint64_t nodes;
            Ply sel_depth;
            Ply min_depth;
            bool only_one_move;
        };

        constexpr Ply null_move_depth_reduction = 4;
        constexpr Score futility_margin[] = {0, 200, 400, 800};
        constexpr Score delta_margin = 150;

        Score quiesce(SearchStruct& ss, const Position& position, Score alpha, Score beta, const Ply height)
        {
            infoassert(alpha < beta);


            if (height >= max_depth)
            {
                return score_draw;
            }

            Score stand_pat;
            Move move{};
            Score score;
            Position new_position{};
            MoveSelector ms;

            int move_counter = 0;

            if (ss.stop)
            {
                return no_score;
            }

            ss.nodes += 1;

            if (position.insufficientMaterial() && height > 0)
            {
                return score_draw;
            }

            bool in_check = position.inCheck(position.us, position.enemy);

            stand_pat = position.evaluate();
            if (stand_pat >= beta)
            {
                return beta;
            }
            if (stand_pat > alpha && !in_check)
            {
                alpha = stand_pat;
            }

            ms.init(position);

            while (ms.next(move, !in_check))
            {
                new_position = position;
                new_position.doMove(move);
                if (new_position.inCheck(new_position.enemy, new_position.us))
                {
                    continue;
                }
                move_counter += 1;

                if (!in_check && stand_pat + position.see(move) + delta_margin < alpha)
                {
                    continue;
                }

                score = -quiesce(ss, new_position, -beta, -alpha, height + 1);


                if (score >= beta)
                {
                    return beta;
                }
                if (score > alpha)
                {
                    alpha = score;
                }
            }

            if (move_counter == 0 && in_check)
            {
                alpha = -score_of_all_pieces;
            }
            return alpha;
        }

        Score search(SearchStruct& ss, const Position& position, Ply depth, const Ply height, Score alpha, Score beta)
        {
            infoassert(alpha < beta);
            infoassert(height < max_depth);

            ss.hash_table.prefetch(position.zobrist_key);

            Position new_position{};
            Move move{};
            Score score;
            MoveSelector ms;
            bool giving_check;

            const bool in_check = position.inCheck(position.us, position.enemy);
            NodeType node_type = all_node;
            Move best_move = no_move;
            Score best_score = -score_infinity;
            int move_counter = 0;
            int lmr_move_counter = 0;
            bool do_futility_pruning = false;

            if (ss.stop)
            {
                return no_score;
            }

            ss.nodes += 1;

            // check for repetition
            if (ss.game_history.checkForRepetition(position.zobrist_key, height) && height > 0)
            {
                return score_draw;
            }
            ss.game_history.update(position.zobrist_key, height);

            if (in_check)
            {
                depth += 1;
            }

            // get transposition table entry
            auto hash_result = ss.hash_table.get(position.zobrist_key);
            if (!hash_result.isEmpty() && hash_result.depth >= depth)
            {
                if (hash_result.node_type == exact)
                {
                    return hash_result.score;
                }
                else if (hash_result.node_type == lower_bound && alpha < hash_result.score)
                {
                    alpha = hash_result.score;
                }
                else if (hash_result.node_type == upper_bound && beta > hash_result.score)
                {
                    beta = hash_result.score;
                }
                if (alpha >= beta)
                {
                    return alpha;
                }
            }

            if (depth <= 0)
            {
                ss.sel_depth = std::max(ss.sel_depth, height);
                ss.min_depth = std::min(ss.min_depth, height);
                return quiesce(ss, position, alpha, beta, height);
            }

            // null move reduction
            const int number_major_minor_pieces = bitboard::popcount(
                (
                    position.pieces[knight] |
                    position.pieces[bishop] |
                    position.pieces[rook] |
                    position.pieces[queen]
                ) & position.players[position.us]
            );
            if (
                height > 0 &&
                !in_check &&
                number_major_minor_pieces >= 2
                )
            {
                new_position = position;
                new_position.doNullMove();

                // somehow height + 3 is better than height + 1
                score = -search(ss, new_position, depth - null_move_depth_reduction - 1, height + 3, -beta, -beta + 1);

                if (score >= beta)
                {
                    depth = depth - null_move_depth_reduction;
                    alpha = score;
                    node_type = cut_node;
                    goto Search_Return;
                }
            }

            // check if futility pruning is applicable
            if (
                beta == alpha + 1 &&
                depth <= 3 &&
                !in_check &&
                std::abs(alpha) < score_of_all_pieces &&
                position.evaluate() + futility_margin[depth] < alpha
                )
            {
                do_futility_pruning = true;
            }

            // initialize move generation, ordering and selection process
            ms.init(
                position,
                hash_result.best_move,
                ss.killer.get(height),
                [&ss](const Move cur_move, const Player player)
                { return ss.history.get_score(cur_move, player); }
            );

            // generate moves and loop over them
            while (ms.next(move))
            {
                new_position = position;
                new_position.doMove(move);

                // check for move legality
                if (new_position.inCheck(new_position.enemy, new_position.us))
                {
                    continue;
                }
                move_counter += 1;

                giving_check = new_position.inCheck(new_position.us, new_position.enemy);

                // futility pruning
                if (
                    do_futility_pruning &&
                    !move.isTactical() &&
                    !giving_check
                    )
                {
                    continue;
                }

                Ply new_depth = depth;
                Score new_alpha = alpha;
                Score new_beta = beta;

                // pvs
                if (alpha > -score_infinity)
                {
                    new_beta = alpha + 1;
                }

                //TODO: time management, fix general format, join all threads

                // late move reduction
                if (
                    new_depth >= 2 &&
                    move_counter > 3 &&
                    !move.isTactical() &&
                    !in_check &&
                    !giving_check &&
                    !(move.moved() == pawn && new_position.isPassedPawn(position.us, position.enemy, move.to()))
                    )
                {
                    static constexpr Ply depth_divider[30] =
                        {60, 30, 25, 20, 15, 10, 9, 8, 7, 6, 6, 5, 5, 5,
                         4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};

                    new_depth -= 1 + new_depth / depth_divider[std::min(lmr_move_counter, 29)];
                    lmr_move_counter += 1;
                }

                score = -search(ss, new_position, new_depth - 1, height + 1, -new_beta, -new_alpha);

                if (score > alpha && (new_alpha > alpha || new_beta < beta)) // re-search full window reduced depth
                {
                    score = -search(ss, new_position, new_depth - 1, height + 1, -beta, -alpha);
                }
                if (score > alpha && new_depth < depth) // re-search full window full depth
                {
                    score = -search(ss, new_position, depth - 1, height + 1, -beta, -alpha);
                }

                if (score > best_score)
                {
                    best_score = score;
                    best_move = move;
                }
                if (score >= beta)
                {
                    node_type = cut_node;
                    alpha = score;
                    goto Search_Return;
                }
                if (score > alpha)
                {
                    node_type = pv_node;
                    alpha = score;
                }
            }

            // if no legal moves have been found
            if (move_counter == 0)
            {
                if (in_check) // checkmate
                {
                    alpha = -(score_mate + (max_depth - height));
                }
                else // stalemate
                {
                    alpha = score_draw;
                }
            }
            else if (height == 0 && move_counter == 1)
            {
                ss.only_one_move = true;
            }

            Search_Return:

            if (!ss.stop)
            {
                // update killers and history heuristic
                if (node_type != all_node && move_counter > 0)
                {
                    ss.history.update(best_move, position.us, depth);
                }
                if (node_type == cut_node && move_counter > 0)
                {
                    ss.killer.update(height, best_move);
                }

                // add the results to the transposition table
                ss.hash_table.add(position.zobrist_key, node_type, alpha, depth, best_move);
            }

            return alpha;
        }

        std::string getInfoString(
            const Ply depth,
            const Ply sel_depth,
            const Ply min_depth,
            const int time_elapsed,
            const uint64_t nodes,
            const Score alpha,
            const std::vector<Move>& pv,
            const int hash_full,
            const float branching_factor
        )
        {
            std::string ret;
            ret += "info";
            ret += " depth " + std::to_string(depth);
            ret += " seldepth " + std::to_string(sel_depth);
            ret += " mindepth " + std::to_string(min_depth);
            ret += " time " + std::to_string(time_elapsed);
            ret += " nodes " + std::to_string(nodes);
            ret += " nps " + std::to_string((int) (nodes / (0.001 * time_elapsed + 0.000001)));
            ret += " ebf " + std::to_string(branching_factor);
            if (hash_full > 0)
            {
                ret += " hashfull " + std::to_string(hash_full);
            }

            if (alpha >= score_mate)
            {
                ret += " score mate " + std::to_string((-(alpha - (score_mate + max_depth)) + 1) / 2);
            }
            else if (alpha <= -score_mate)
            {
                ret += " score mate " + std::to_string(((-alpha - (score_mate + max_depth)) - 1) / 2);
            }
            else
            {
                ret += " score cp " + std::to_string(alpha);
            }
            ret += " pv ";
            ret += util::getString(pv);
            return ret;
        }

        std::pair<Move, Score> rootSearch(
            const Position& position,
            HashTable& hash_table,
            const std::vector<Position>& game_history,
            const Ply target_depth,
            const int move_time,
            const std::atomic<bool>& stop,
            std::ostream& output
        )
        {
            std::chrono::steady_clock::time_point start_time = std::chrono::steady_clock::now();

            SearchStruct ss =
                {
                    hash_table,
                    KillerTable(),
                    HistoryTable(),
                    GameHistory(game_history),
                    stop,
                    0,
                    0,
                    max_depth,
                    false
                };

            uint64_t old_nodes = -1;
            float branching_factors[max_depth];
            Move best_move = no_move;
            Score score = no_score;
            ss.hash_table.age();
            for (Ply depth = 1; depth <= target_depth && depth < max_depth; depth++)
            {
                std::chrono::steady_clock::time_point iteration_start_time = std::chrono::steady_clock::now();

                ss.nodes = 0;
                ss.sel_depth = 0;
                ss.min_depth = max_depth;
                score = search(ss, position, depth, 0, -score_infinity, score_infinity);

                if (ss.stop)
                {
                    break;
                }
                else
                {
                    if (ss.hash_table.getPv(position).empty())
                    {
                        ss.hash_table.clear();
                        output << "info Cleared TT" << std::endl;
                        score = search(ss, position, depth, 0, -score_infinity, score_infinity);
                        infoassert(!ss.hash_table.getPv(position).empty());
                    }
                    best_move = ss.hash_table.getPv(position).front();
                }

                std::chrono::steady_clock::time_point end_time = std::chrono::steady_clock::now();
                int time_last_iteration = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - iteration_start_time).count();
                int time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count();


                branching_factors[depth] = (float) ss.nodes / (float) old_nodes;
                if (old_nodes == 1)
                {
                    branching_factors[depth] = 1.0;
                }

                output << getInfoString(
                    depth,
                    ss.sel_depth,
                    ss.min_depth,
                    time_last_iteration,
                    ss.nodes,
                    score,
                    ss.hash_table.getPv(position),
                    ss.hash_table.hashFull(),
                    branching_factors[depth]
                ) << std::endl;

                if (score >= score_mate || score <= -score_mate || ss.only_one_move)
                {
                    ss.hash_table.clear();
                    break;
                }

                if (move_time >= 0)
                {
                    float averanged_branching_factor = branching_factors[depth];
                    if (depth >= 4)
                    {
                        averanged_branching_factor =
                            (branching_factors[depth] + branching_factors[depth - 1] + branching_factors[depth - 2] + branching_factors[depth - 3]) /
                            4.0;
                    }

                    auto estimated_time_next_iteration = time_last_iteration * averanged_branching_factor * 1.8;
                    if (
                        (estimated_time_next_iteration > move_time && depth >= 6) ||
                        time_elapsed > move_time
                        )
                    {
                        break;
                    }
                }

                old_nodes = ss.nodes;
            }

            output << "bestmove " + best_move.getNotation() << std::endl;
            return std::make_pair(best_move, score);
        }

        auto calculateMoveTime(
            const int milliseconds_left,
            const int milliseconds_inc_per_move,
            const int moves_to_go,
            const int fullmoves_played
        )
        {
            const int estimated_game_length = 80;
            int new_moves_to_go = moves_to_go;
            if (new_moves_to_go == -1)
            {
                new_moves_to_go = std::max(estimated_game_length - fullmoves_played, 20);
            }
            else if (new_moves_to_go > std::max(estimated_game_length - fullmoves_played, 30))
            {//30 != 20 is not a bug, moves_to_go is safer to use so i want to use it more often than estimated moves_to_go
                new_moves_to_go = std::max(estimated_game_length - fullmoves_played, 20);
            }

            if (new_moves_to_go <= 40)
            {
                new_moves_to_go = 2 + (100 * new_moves_to_go) / 105;
            }

            struct Ret
            {
                int max;
                int approx;
            };
            Ret ret
                {
                    milliseconds_left / 2,
                    std::clamp(milliseconds_left / std::max(new_moves_to_go, 1), 0, INT32_MAX / 2) +
                    std::clamp(milliseconds_inc_per_move, 0, INT32_MAX / 2)
                };
            return ret;
        }
    }

    std::pair<Move, Score> go(
        const Position& position,
        HashTable& hash_table,
        const std::vector<Position>& game_history,
        const Ply target_depth,
        const int ms_left,
        const int ms_inc_per_move,
        const int moves_to_go,
        std::atomic<bool>& stop,
        std::ostream& output
    )
    {

        auto move_time = calculateMoveTime(ms_left, ms_inc_per_move, moves_to_go, position.fullmoves_played);
        std::thread stop_watch;
        if (ms_left >= 0)
        {
            stop_watch = std::thread(util::setIn, move_time.max, std::ref(stop));
        }

        auto ret = rootSearch(position, hash_table, game_history, target_depth, move_time.approx, stop, output);


        stop = true;
        if (stop_watch.joinable())
        {
            stop_watch.join();
        }

        return ret;
    }
}

uint64_t perft::perft(const int depth, const Position& position)
{

    if (depth == 0)
    {
        return 1;
    }

    uint64_t nodes = 0;

    search::MoveSelector ms;
    ms.init(position);
    //Move moves[max_num_moves];
    //auto num_moves = position.generate_moves(moves);
    Move move = no_move;
    Move best_move{};
    while (ms.next(move))
        //for(int i = 0; i< num_moves; ++i)
    {
        Position new_position = position;
        //new_position.do_move(moves[i]);
        new_position.doMove(move);
        if (!new_position.inCheck(position.us, position.enemy))
        {
            if (new_position.zobrist_key != new_position.getZobristKey())
            {
                std::cout << position.getString() << std::endl;
                std::cout << position.debugString() << std::endl;
                std::cout << new_position.getString() << std::endl;
                std::cout << new_position.debugString() << std::endl;
                std::cout << move.getString() << std::endl;
                std::cout << move.getNotation() << std::endl;
                std::cout << "NO_MOVE: " << no_move.getString() << std::endl;
                std::cout << "NO_MOVE: " << no_move.getNotation() << std::endl;
                infoassert(position.isPseudoLegal(move));
                infoassert(false);
            }
            best_move = move;
            nodes += perft(depth - 1, new_position);
        };
    }
    return nodes;
}
