#pragma once

#include <cstdint>

using Player = int8_t;
enum : Player
{
    white, black, no_player
};

constexpr Player switchPlayer(const Player player)
{ return player ^ black; }

using Piece = int8_t;
enum : Piece
{
    pawn, knight, bishop, rook, queen, king, no_piece
};

using Ply = int8_t;
constexpr Ply max_depth = INT8_MAX;

using Score = int16_t;
constexpr Score score_pawn = 100;
constexpr Score score_knight = 320;
constexpr Score score_bishop = 330;
constexpr Score score_rook = 510;
constexpr Score score_queen = 980;
constexpr Score score_king = 0;
constexpr Score score_no_piece = 0;
constexpr Score score_list[7] = {score_pawn, score_knight, score_bishop, score_rook, score_queen, score_king, score_no_piece};
constexpr Score score_of_all_pieces = (8 * score_pawn + 2 * score_knight + 2 * score_bishop + 2 * score_rook + score_queen) * 2;
constexpr Score score_mate = score_of_all_pieces * 2 + max_depth;
constexpr Score score_draw = 0;
constexpr Score score_infinity = INT16_MAX;
constexpr Score no_score = INT16_MIN;
static_assert(no_score < -score_infinity);


enum NodeType
{
    pv_node, all_node, cut_node, no_node
};
constexpr NodeType exact = pv_node;
constexpr NodeType upper_bound = all_node;
constexpr NodeType lower_bound = cut_node;

using Square = uint8_t;
enum : Square
{
    a1, b1, c1, d1, e1, f1, g1, h1,
    a2, b2, c2, d2, e2, f2, g2, h2,
    a3, b3, c3, d3, e3, f3, g3, h3,
    a4, b4, c4, d4, e4, f4, g4, h4,
    a5, b5, c5, d5, e5, f5, g5, h5,
    a6, b6, c6, d6, e6, f6, g6, h6,
    a7, b7, c7, d7, e7, f7, g7, h7,
    a8, b8, c8, d8, e8, f8, g8, h8,
    no_square
};

using Phase = int8_t;
constexpr Phase opening = 32;
constexpr Phase endgame = 0;

constexpr uint8_t max_num_captures = 64;
constexpr uint8_t max_num_quiets = 96;
constexpr uint8_t max_num_moves = max_num_captures + max_num_quiets;

enum EndOfGame
{
    white_win, black_win, draw, not_ended
};
constexpr EndOfGame win[2] = {white_win, black_win};
