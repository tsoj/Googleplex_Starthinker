#include "bitboard.hpp"
#include "Position.hpp"
#include "infoassert.hpp"

#include <array>

namespace
{
    Score pstScore(const Player us, const Piece piece, const Square square, const Phase phase)
    {
        using Pst = std::array<std::array<std::array<std::array<Score, 64>, 2>, 6>, 2>;
        auto gen_pst = []()
        {
            const Score pst_black[6][2][64] =
                {
                    // PAWN
                    {
                        // OPENING
                        {
                            0,   0,   0,   0,   0,   0,   0,   0,
                            45,  45,  45,  45,  45,  45,  45,  45,
                            10,  10,  20,  30,  30,  20,  10,  10,
                            5,   5,   10,  25,  25,  10,  5,   5,
                            0,   0,   0,   20,  20,  0,   0,   0,
                            5,   -5,  -10, 0,   0,   -10, -5,  5,
                            5,   10,  10, -20, -20, 10, 10,  5,
                            0,   0,   0,   0,   0,   0,   0,   0
                        },
                        // ENDGAME
                        {
                            0,   0,   0,   0,   0,   0,   0,   0,
                            90,  90,  90,  90, 90, 90,  90,  90,
                            30,  30,  40, 45, 45, 40, 40,  30,
                            20,  20,  20, 25, 25, 20, 20,  20,
                            0,   0,   0,  20, 20, 0,  0,   0,
                            -5,  -5,  -10, -10, -10, -10, -5,  -5,
                            -15, -15, -15, -20, -20, -15, -15, -15,
                            0,   0,   0,   0,   0,   0,   0,   0
                        }
                    },
                    // KNIGHT
                    {
                        // OPENING
                        {
                            -50, -40, -30, -30, -30, -30, -40, -50,
                            -40, -20, 0,   0,   0,   0,   -20, -40,
                            -30, 0,   10,  15,  15,  10,  0,   -30,
                            -30, 5,   15,  20,  20,  15,  5,   -30,
                            -30, 0,   15,  20,  20,  15,  0,   -30,
                            -30, 5,   10,  15,  15,  10,  5,   -30,
                            -40, -20, 0,  5,   5,   0,  -20, -40,
                            -50, -40, -30, -30, -30, -30, -40, -50,
                        },
                        // ENDGAME
                        {
                            -50, -40, -30, -30, -30, -30, -40, -50,
                            -40, -20, 0,   0,  0,  0,   -20, -40,
                            -30, 0,   10, 15, 15, 10, 0,   -30,
                            -30, 5,   15, 20, 20, 15, 5,   -30,
                            -30, 0,   15, 20, 20, 15, 0,   -30,
                            -30, 5,   10,  15,  15,  10,  5,   -30,
                            -40, -20, 0,   5,   5,   0,   -20, -40,
                            -50, -40, -30, -30, -30, -30, -40, -50,
                        }
                    },
                    // BISHOP
                    {
                        // OPENING
                        {
                            -20, -10, -10, -10, -10, -10, -10, -20,
                            -10, 0,   0,   0,   0,   0,   0,   -10,
                            -10, 0,   5,   10,  10,  5,   0,   -10,
                            -10, 5,   5,   10,  10,  5,   5,   -10,
                            -10, 0,   10,  10,  10,  10,  0,   -10,
                            -10, 10,  10,  10,  10,  10,  10,  -10,
                            -10, 5,   0,  0,   0,   0,  5,   -10,
                            -20, -10, -10, -10, -10, -10, -10, -20,
                        },
                        // ENDGAME
                        {
                            -20, -10, -10, -10, -10, -10, -10, -20,
                            -10, 0,   0,   0,  0,  0,   0,   -10,
                            -10, 0,   5,  10, 10, 5,  0,   -10,
                            -10, 5,   5,  10, 10, 5,  5,   -10,
                            -10, 0,   10, 10, 10, 10, 0,   -10,
                            -10, 10,  10,  10,  10,  10,  10,  -10,
                            -10, 5,   0,   0,   0,   0,   5,   -10,
                            -20, -10, -10, -10, -10, -10, -10, -20,
                        }
                    },
                    // ROOK
                    {
                        // OPENING
                        {
                            0,   0,   0,   0,   0,   0,   0,   0,
                            5,   10,  10,  10,  10,  10,  10,  5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            -5,  0,   0,  0,   0,   0,  0,   -5,
                            0,   0,   0,   5,   5,   0,   0,   0
                        },
                        // ENDGAME
                        {
                            0,   0,   0,   0,   0,   0,   0,   0,
                            0,   5,   5,   5,  5,  5,   5,   0,
                            -5,  0,   0,  0,  0,  0,  0,   -5,
                            -5,  0,   0,  0,  0,  0,  0,   -5,
                            -5,  0,   0,  0,  0,  0,  0,   -5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            -5,  0,   0,   0,   0,   0,   0,   -5,
                            0,   0,   0,   0,   0,   0,   0,   0
                        }
                    },
                    // QUEEN
                    {
                        // OPENING
                        {
                            -20, -10, -10, -5,  -5,  -10, -10, -20,
                            -10, 0,   0,   0,   0,   0,   0,   -10,
                            -10, 0,   5,   5,   5,   5,   0,   -10,
                            -5,  0,   5,   5,   5,   5,   0,   -5,
                            0,   0,   5,   5,   5,   5,   0,   -5,
                            -10, 5,   5,   5,   5,   5,   0,   -10,
                            -10, 0,   5,  0,   0,   0,  0,   -10,
                            -20, -10, -10, -5,  -5,  -10, -10, -20
                        },
                        // ENDGAME
                        {
                            -20, -10, -10, -5,  -5,  -10, -10, -20,
                            -10, 0,   0,   0,  0,  0,   0,   -10,
                            -10, 0,   5,  5,  5,  5,  0,   -10,
                            -5,  0,   5,  5,  5,  5,  0,   -5,
                            0,   0,   5,  5,  5,  5,  0,   -5,
                            -10, 0,   5,   5,   5,   5,   0,   -10,
                            -10, 0,   0,   0,   0,   0,   0,   -10,
                            -20, -10, -10, -5,  -5,  -10, -10, -20
                        }
                    },
                    // KING
                    {
                        // OPENING
                        {
                            -30, -40, -40, -50, -50, -40, -40, -30,
                            -30, -40, -40, -50, -50, -40, -40, -30,
                            -30, -40, -40, -50, -50, -40, -40, -30,
                            -30, -40, -40, -50, -50, -40, -40, -30,
                            -20, -30, -30, -40, -40, -30, -30, -20,
                            -10, -20, -20, -20, -20, -20, -20, -10,
                            20,  20,  0,  0,   0,   0,  20,  20,
                            20,  30,  10,  0,   0,   10,  30,  20
                        },
                        // ENDGAME
                        {
                            -50, -40, -30, -20, -20, -30, -40, -50,
                            -30, -20, -10, 0,  0,  -10, -20, -30,
                            -30, -10, 20, 30, 30, 20, -10, -30,
                            -30, -10, 30, 40, 40, 30, -10, -30,
                            -30, -10, 30, 40, 40, 30, -10, -30,
                            -30, -10, 20,  30,  30,  20,  -10, -30,
                            -30, -30, 0,   0,   0,   0,   -30, -30,
                            -50, -30, -30, -30, -30, -30, -30, -50
                        }
                    },
                };
            Pst ret = {};
            for (Piece cur_piece = 0; cur_piece < no_piece; cur_piece++)
            {
                for (Phase cur_phase = 0; cur_phase < 2; cur_phase++)
                {
                    for (Square cur_square = 0; cur_square < no_square; cur_square++)
                    {
                        ret[black][cur_piece][cur_phase][cur_square] = pst_black[cur_piece][cur_phase][cur_square];
                        ret[white][cur_piece][cur_phase][cur_square] = pst_black[cur_piece][cur_phase][util::mirror(cur_square)];
                    }
                }
            }
            return ret;
        };
        static const Pst pst = gen_pst();
        return (pst[us][piece][0][square] * (phase)) / 32 + (pst[us][piece][1][square] * (32 - phase)) / 32;
    }

    Score reachableSquares(const Piece piece, const Square square, const Position& position, const Player us)
    {
        const uint64_t occupancy = position.players[white] | position.players[black];
        return bitboard::popcount((bitboard::attackMask(piece, square, occupancy)) & ~(position.players[us]));
    }

    // returns 0 if not, 1 if outpost, 2 if outpost and no minor attacker
    int isOutpost(const Position& position, const Square square, const Player us, const Player enemy)
    {
        if (
            position.isPassedPawn(us, enemy, square) &&
                (bitboard::attackMaskPawnCapture(enemy, square) & position.players[us] & position.pieces[pawn]) != 0
            )
        {
            if ((position.players[enemy] & position.pieces[knight]) == 0)
            {
                const uint64_t enemy_bishops = position.players[enemy] & position.pieces[bishop];
                if (
                    enemy_bishops == 0 ||
                    (
                        bitboard::popcount(enemy_bishops) == 1 &&
                            util::getSquareColor(bitboard::ctz(enemy_bishops)) != util::getSquareColor(square)
                    )
                    )
                {
                    return 2;
                }
            }
            return 1;
        }
        return 0;
    }


    constexpr Score bonusPassedPawn(const Phase phase, const Square square, const Player us)
    {
        constexpr Score opening_table[8] = {0, 0, 0, 10, 15, 20, 45, 0};
        constexpr Score engame_table[8] = {0, 10, 15, 20, 30, 40, 90, 0};
        if (us == white)
        {
            return (opening_table[square / 8] * (phase)) / 32 + (engame_table[square / 8] * (32 - phase)) / 32;
        }
        infoassert(us == black);
        return (opening_table[7 - square / 8] * (phase)) / 32 + (engame_table[7 - square / 8] * (32 - phase)) / 32;
    }

    constexpr Score penalty_isolated_pawn = 10;
    constexpr Score penalty_backward_pawn = 0;//2; //backward pawn doesn't seem to work.
    constexpr Score penalty_double_pawn = 0;//5; // double_pawns doesn't seem to work.
    constexpr Score bonus_both_bishops = 10;
    constexpr Score bonus_rooks_are_connected = 0;//2; // doesn't change anything
    constexpr Score bonus_rook_on_open_file = 5;
    constexpr Score bonus_knight_outpost = 0;//4; // simple knight outpost doesn't seem to work.
    constexpr Score bonus_extra_knight_outpost_no_attackers = 6; // made no real difference
    constexpr Score bonus_bishop_outpost = 2; //made no real difference
    constexpr Score bonus_extra_bishop_outpost_no_attackers = 0;//4; full bishop outpost didn't really work
    constexpr float mobility_multiplier_bishop = 3.0;
    constexpr float mobility_multiplier_rook = 4.0;
    constexpr float mobility_multiplier_queen = 2.0;
    constexpr Score penalty_rook_second_rank_from_king = 10;
    constexpr Score penalty_enemy_piece_near_king[7] = {0, 0, 0, 0, 10, 20, 30};


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

    Score evaluatePawn(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;
        // passed pawn
        if (position.isPassedPawn(us, enemy, square))
        {
            ret += bonusPassedPawn(phase, square, us);
        }
        // isolated pawn
        if (
            (square % 8 == 0 || position.pieces[pawn] & position.players[us] & bitboard::file(square - 1)) == 0 &&
            (square % 8 == 7 || position.pieces[pawn] & position.players[us] & bitboard::file(square + 1)) == 0
            )
        {
            ret -= penalty_isolated_pawn;
        }
            // backward pawn
        else
        {
            const uint64_t stop_square_bb = bitboard::attackMaskPawnQuiet(us, square);
            const uint64_t enemy_pawns = position.pieces[pawn] & position.players[enemy];
            const uint64_t our_pawns = position.pieces[pawn] & position.players[us];
            if (
                (// is the stop square not free/(attacked and not defended)?
                    (stop_square_bb & enemy_pawns) != 0 ||
                        (bitboard::attackMaskPawnCapture(us, bitboard::ctz(stop_square_bb)) & enemy_pawns) != 0
                ) &&
                // is our pawn here and on the stop square not defendable?
                (our_pawns & bitboard::isPassed(enemy, bitboard::ctz(stop_square_bb)) & ~bitboard::file(square)) == 0
                )
            {
                ret -= penalty_backward_pawn;
            }
        }
        // double pawn
        if (((position.pieces[pawn] & position.players[us] & bitboard::file(square - 1)) ^ bitboard::bitAtIndex(square)) != 0)
        {
            ret -= penalty_double_pawn;
        }


        return ret;
    }

    Score evaluateKnight(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;
        int is_knight_outpost = isOutpost(position, square, us, enemy);
        if (is_knight_outpost != 0)
        {
            ret += bonus_knight_outpost;
        }
        if (is_knight_outpost == 2)
        {
            ret += bonus_extra_knight_outpost_no_attackers;
        }
        return ret;
    }

    Score evaluateBishop(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;
        if ((position.players[us] & position.pieces[bishop] & ~bitboard::bitAtIndex(square)) != 0)
        {
            ret += bonus_both_bishops;
        }
        ret += mobility_multiplier_bishop * reachableSquares(bishop, square, position, us);
        int is_bishop_outpost = isOutpost(position, square, us, enemy);
        if (is_bishop_outpost != 0)
        {
            ret += bonus_bishop_outpost;
        }
        if (is_bishop_outpost == 2)
        {
            ret += bonus_extra_bishop_outpost_no_attackers;
        }
        return ret;
    }

    Score evaluateRook(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;
        //connected rooks are better
        const uint64_t attack_mask = bitboard::attackMask(rook, square, position.players[white] | position.players[black]);
        if ((attack_mask & position.pieces[rook] & position.players[us] & ~bitboard::bitAtIndex(square)) != 0)
        {
            ret += bonus_rooks_are_connected;
        }
        // rook on open file
        if ((bitboard::file(square) & position.pieces[pawn]) == 0)
        {
            ret += bonus_rook_on_open_file;
        }
        ret += mobility_multiplier_rook * reachableSquares(rook, square, position, us);
        return ret;
    }

    Score evaluateQueen(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;
        ret += mobility_multiplier_queen * reachableSquares(queen, square, position, us);
        return ret;
    }

    Score evaluateKing(const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        Score ret = 0;

        //Didn't work
        ret -= penalty_enemy_piece_near_king[std::min(bitboard::popcount(bitboard::getMask(3, square) & position.players[enemy]), 6)];

        // rook on second rank/file is bad
        const uint64_t enemy_rooks = position.pieces[rook] & position.players[enemy];
        if
            (
            (
                (bitboard::rank(square) & bitboard::rank(a1)) != 0 &&
                (bitboard::rank(a2) & enemy_rooks) != 0
            ) ||
            (
                (bitboard::rank(square) & bitboard::rank(a8)) != 0 &&
                (bitboard::rank(a7) & enemy_rooks) != 0
            ) ||
            (
                (bitboard::file(square) & bitboard::file(a1)) != 0 &&
                (bitboard::file(b1) & enemy_rooks) != 0
            ) ||
            (
                (bitboard::file(square) & bitboard::file(h1)) != 0 &&
                (bitboard::file(g1) & enemy_rooks) != 0
            )
            )
        {
            ret -= penalty_rook_second_rank_from_king;
        }
        return ret;
    }

#pragma GCC diagnostic pop

    Score evaluatePiece(const Piece piece, const Position& position, const Square square, const Player us, const Player enemy, const Phase phase)
    {
        constexpr Score (* evaluation_functions[])(const Position&, const Square, const Player, const Player, const Phase) =
            {
                evaluatePawn,
                evaluateKnight,
                evaluateBishop,
                evaluateRook,
                evaluateQueen,
                evaluateKing,
            };
        return evaluation_functions[piece](position, square, us, enemy, phase);
    }

    Score evaluatePieceType(const Piece piece, const Position& position, const Phase phase)
    {
        const Player us = position.us;
        const Player enemy = position.enemy;
        Score ret = 0;

        uint64_t tmp_occupancy = position.pieces[piece];
        while (tmp_occupancy != 0)
        {
            const Square square = bitboard::rto(tmp_occupancy);
            if ((bitboard::bitAtIndex(square) & position.players[us]) != 0)
            {
                ret += score_list[piece];
                ret += pstScore(us, piece, square, phase);
                ret += evaluatePiece(piece, position, square, us, enemy, phase);
            }
            else
            {
                ret -= score_list[piece];
                ret -= pstScore(enemy, piece, square, phase);
                ret -= evaluatePiece(piece, position, square, enemy, us, phase);
            }
        }
        return ret;
    }
}

Score Position::evaluate() const
{
    if (halfmove_clock >= 100)
    {
        return score_draw;
    }
    Score score = 0;
    Phase phase = getPhase();
    for (Piece piece = 0; piece < no_piece; ++piece)
    {
        score += evaluatePieceType(piece, *this, phase);
    }
    return score;
}
