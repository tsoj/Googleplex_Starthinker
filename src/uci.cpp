#include "uci.hpp"
#include "Position.hpp"
#include "HashTable.hpp"
#include "search.hpp"
#include "perft.hpp"

#include <iostream>
#include <vector>
#include <atomic>
#include <thread>
#include <optional>
#include <memory>


namespace uci
{
    const std::string chess_engine_version = "1.7beta-17";
    const std::string chess_engine_name = "Googleplex Starthinker";
    const std::string chess_engine_logo =
        "     ___________   ___________\n\
    /  _____   /   \\   _______\\      /\\\n\
   /  /    /__/     \\  \\_______    <    >\n\
  /  /    ___        \\_______  \\     \\/\n\
 /  /____/  /         _______\\  \\\n\
/_______   /oogleplex \\__________\\tarthinker\n\
       /  /\n\
   ___/  /\n\
  /_____/\n";
    const std::string chess_engine_author = "Jost Triller";
    const std::string startpos_fen = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
    constexpr size_t default_hash_size = 140;
    constexpr size_t mb = 1'000'000;
    const std::string uci_help = "\n\
You can use the universal chess interface (UCI): \n\
https://web.archive.org/web/20180216142337/http://wbec-ridderkerk.nl/html/UCIProtocol.html \n\
\n\
Additional commands:\n\
\n\
* print\n\
  This will print the internal chessposition.\n\
\n\
* printdebug\n\
  This will print the exact bitboards of the internal chessposition.\n\
\n\
* getfen\n\
  This will print the FEN notation of the internal chessposition.\n\
\n\
* help\n\
  Prints this text.\n\
\n\
* perft\n\
  Runs perft test.\n\
\n\
* moves ...\n\
  Plays the moves on the internal chess board.\n";

    namespace uci_state
    {
        HashTable hash_table = HashTable(default_hash_size * mb);
        std::atomic<bool> stop = true;
        std::vector<Position> game_history = std::vector<Position>();
        Position position = Position::fen(startpos_fen);
    }

    void uci()
    {
        std::cout << "id name " << chess_engine_name << " " << chess_engine_version << std::endl;
        std::cout << "id author " << chess_engine_author << std::endl;
        std::cout << "option name Hash type spin default " << default_hash_size << " min 1 max 1048576" << std::endl;
        std::cout << "uciok" << std::endl;
    }

    void setoption(const std::vector<std::string>& params)
    {
        if (
            params.size() == 5 &&
            params[1] == "name" &&
            params[2] == "Hash" &&
            params[3] == "value"
            )
        {
            size_t new_hash_size = std::strtoull(params[4].c_str(), nullptr, 10);
            if (new_hash_size < 1 || new_hash_size > 1048576)
            {
                std::cout << "Invalid value" << std::endl;
            }
            else
            {
                uci_state::hash_table = HashTable(new_hash_size * mb);
            }
        }
        else
        {
            std::cout << "Unkown parameters" << std::endl;
        }
    }

    void stop()
    {
        uci_state::stop = true;
    }

    void position(const std::vector<std::string>& params)
    {
        uci_state::game_history = std::vector<Position>();
        size_t index = 0;
        if (params.size() >= 2 && params[1] == "startpos")
        {
            uci_state::position = Position::fen(startpos_fen);
            uci_state::game_history.push_back(uci_state::position);
            index = 2;
        }
        else if (params.size() >= 8 && params[1] == "fen")
        {
            std::string fen = params[2] + " " + params[3] + " " + params[4] + " " + params[5] + " " + params[6] + " " + params[7];
            uci_state::position = Position::fen(fen);
            uci_state::game_history.push_back(uci_state::position);
            index = 8;
        }
        else
        {
            std::cout << "Unkown parameter" << std::endl;
            return;
        }

        if (params.size() >= index + 1 && params[index] == "moves")
        {
            for (size_t i = index + 1; i < params.size(); i++)
            {
                Move move = util::getMove(params[i], uci_state::position);
                uci_state::position.doMove(move);
                uci_state::game_history.push_back(uci_state::position);
            }
        }
        else if (params.size() >= 9)
        {
            std::cout << "Unkown parameter" << std::endl;
        }
    }

    void moves(const std::vector<std::string>& params)
    {
        for (size_t i = 1; i < params.size(); i++)
        {
            Move move = util::getMove(params[i], uci_state::position);
            uci_state::position.doMove(move);
            uci_state::game_history.push_back(uci_state::position);
        }
    }

    void go(const std::vector<std::string>& params)
    {
        Ply depth = max_depth;
        std::optional<int> wtime = {};
        std::optional<int> btime = {};
        int winc = 0;
        int binc = 0;
        int moves_to_go = -1;
        std::optional<int> movetime = {};
        std::optional<int> nodes = {};

        for (size_t i = 1; i + 1 < params.size(); i++)
        {
            if (params[i] == "depth")
            {
                i += 1;
                depth = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "wtime")
            {
                i += 1;
                wtime = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "btime")
            {
                i += 1;
                btime = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "winc")
            {
                i += 1;
                winc = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "binc")
            {
                i += 1;
                binc = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "movestogo")
            {
                i += 1;
                moves_to_go = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "movetime")
            {
                i += 1;
                movetime = std::strtol(params[i].c_str(), NULL, 10);
            }
            else if (params[i] == "nodes")
            {
                i += 1;
                nodes = std::strtol(params[i].c_str(), NULL, 10);
            }
            else
            {
                std::cout << "Unkown parameter" << std::endl;
                return;
            }
        }

        int32_t ms_time_left = INT32_MAX;
        int32_t ms_inc_per_move = 0;
        if (uci_state::position.us == white)
        {
            if (wtime.has_value())
            {
                ms_time_left = wtime.value();
                ms_inc_per_move = winc;
            }
        }
        else if (uci_state::position.us == black)
        {
            if (wtime.has_value())
            {
                ms_time_left = btime.value();
                ms_inc_per_move = binc;
            }
        }

        uci_state::stop = false;

        if (movetime.has_value())
        {
            std::thread stop_watch(util::setIn, movetime.value(), std::ref(uci_state::stop));
            stop_watch.detach();
        }

        std::thread search(search::go,
                           uci_state::position,
                           std::ref(uci_state::hash_table),
                           uci_state::game_history,
                           depth,
                           ms_time_left,
                           ms_inc_per_move,
                           moves_to_go,
                           std::ref(uci_state::stop),
                           std::ref(std::cout)
        );

        search.detach();
    }

    void ucinewgame()
    {
        uci_state::hash_table.clear();
    }

    void uciLoop()
    {
        std::cout << chess_engine_logo << std::endl;
        std::cout << "Copyright (c) 2019 Jost Triller" << std::endl;

        while (true)
        {
            std::string command;
            std::getline(std::cin, command, '\n');

            std::vector<std::string> params = util::split(command, ' ');

            if (params.size() <= 0)
            {
                continue;
            }
            if (params[0] == "uci")
            {
                uci();
            }
            else if (params[0] == "isready")
            {
                std::cout << "readyok" << std::endl;;
            }
            else if (params[0] == "setoption")
            {
                setoption(params);
            }
            else if (params[0] == "position")
            {
                position(params);
            }
            else if (params[0] == "go")
            {
                go(params);
            }
            else if (params[0] == "stop")
            {
                stop();
            }
            else if (params[0] == "quit")
            {
                stop();
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                break;
            }
            else if (params[0] == "print")
            {
                std::cout << uci_state::position.getString() << std::endl;
            }
            else if (params[0] == "printdebug")
            {
                std::cout << uci_state::position.debugString() << std::endl;
            }
            else if (params[0] == "getfen")
            {
                std::cout << uci_state::position.getFen() << std::endl;
            }
            else if (params[0] == "ucinewgame")
            {
                ucinewgame();
            }
            else if (params[0] == "moves")
            {
                moves(params);
            }
            else if (params[0] == "help")
            {
                std::cout << uci_help << std::endl;
            }
            else if (params[0] == "perft")
            {
                perft::test();
            }
            else
            {
                std::cout << "Unknown command: " << params[0] << std::endl;
            }
        }
    }
}
