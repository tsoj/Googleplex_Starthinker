#include "util.hpp"
#include "Position.hpp"
#include "Move.hpp"
#include "bitboard.hpp"
#include "castling.hpp"

#include <thread>
#include <sstream>

namespace
{
#if defined(_MSC_VER)
    const std::string black_circle_unicode = "O";
#else
    const std::string black_circle_unicode = "\u25cf";
#endif
}

namespace util
{
    std::string getUnicodePiece(const Player& player, const Piece& piece)
    {
        if (player == black)
        {
            switch (piece)
            {
#if defined(_MSC_VER)
                case pawn:
                    return "p";
                case knight:
                    return "n";
                case bishop:
                    return "b";
                case rook:
                    return "r";
                case queen:
                    return "q";
                case king:
                    return "k";
                case no_piece:
                    return "-";
                default:
                    return "?";
#else
                case pawn:
                    return "\u2659";
                case knight:
                    return "\u2658";
                case bishop:
                    return "\u2657";
                case rook:
                    return "\u2656";
                case queen:
                    return "\u2655";
                case king:
                    return "\u2654";
                case no_piece:
                    return "-";
                default:
                    return "?";
#endif
            }
        }
        if (player == white)
        {
            switch (piece)
            {
#if defined(_MSC_VER)
                case pawn:
                    return "P";
                case knight:
                    return "N";
                case bishop:
                    return "B";
                case rook:
                    return "R";
                case queen:
                    return "Q";
                case king:
                    return "K";
                case no_piece:
                    return "-";
                default:
                    return "?";
#else
                case pawn:
                    return "\u265F";
                case knight:
                    return "\u265E";
                case bishop:
                    return "\u265D";
                case rook:
                    return "\u265C";
                case queen:
                    return "\u265B";
                case king:
                    return "\u265A";
                case no_piece:
                    return "-";
                default:
                    return "?";
#endif
            }
        }
        if (player == no_player)
        {
            return "-";
        }
        return "?";
    }

    std::string getPieceNotation(const Player& player, const Piece& piece)
    {
        if (player == black)
        {
            switch (piece)
            {
                case pawn:
                    return "p";
                case knight:
                    return "n";
                case bishop:
                    return "b";
                case rook:
                    return "r";
                case queen:
                    return "q";
                case king:
                    return "k";
                case no_piece:
                    return "-";
                default:
                    return "?";
            }
        }
        if (player == white)
        {
            switch (piece)
            {
                case pawn:
                    return "P";
                case knight:
                    return "N";
                case bishop:
                    return "B";
                case rook:
                    return "R";
                case queen:
                    return "Q";
                case king:
                    return "K";
                case no_piece:
                    return "-";
                default:
                    return "?";
            }
        }
        if (player == no_player)
        {
            return "-";
        }
        return "?";
    }

    Square rotateCounterclockwise(const Square& s)
    {
        return Square(8 * (s % 8) + (7 - (s / 8)));
    }

    std::string getSquareNotation(const Square& i)
    {
        switch (i)
        {
            case a1:
                return "a1";
            case b1:
                return "b1";
            case c1:
                return "c1";
            case d1:
                return "d1";
            case e1:
                return "e1";
            case f1:
                return "f1";
            case g1:
                return "g1";
            case h1:
                return "h1";
            case a2:
                return "a2";
            case b2:
                return "b2";
            case c2:
                return "c2";
            case d2:
                return "d2";
            case e2:
                return "e2";
            case f2:
                return "f2";
            case g2:
                return "g2";
            case h2:
                return "h2";
            case a3:
                return "a3";
            case b3:
                return "b3";
            case c3:
                return "c3";
            case d3:
                return "d3";
            case e3:
                return "e3";
            case f3:
                return "f3";
            case g3:
                return "g3";
            case h3:
                return "h3";
            case a4:
                return "a4";
            case b4:
                return "b4";
            case c4:
                return "c4";
            case d4:
                return "d4";
            case e4:
                return "e4";
            case f4:
                return "f4";
            case g4:
                return "g4";
            case h4:
                return "h4";
            case a5:
                return "a5";
            case b5:
                return "b5";
            case c5:
                return "c5";
            case d5:
                return "d5";
            case e5:
                return "e5";
            case f5:
                return "f5";
            case g5:
                return "g5";
            case h5:
                return "h5";
            case a6:
                return "a6";
            case b6:
                return "b6";
            case c6:
                return "c6";
            case d6:
                return "d6";
            case e6:
                return "e6";
            case f6:
                return "f6";
            case g6:
                return "g6";
            case h6:
                return "h6";
            case a7:
                return "a7";
            case b7:
                return "b7";
            case c7:
                return "c7";
            case d7:
                return "d7";
            case e7:
                return "e7";
            case f7:
                return "f7";
            case g7:
                return "g7";
            case h7:
                return "h7";
            case a8:
                return "a8";
            case b8:
                return "b8";
            case c8:
                return "c8";
            case d8:
                return "d8";
            case e8:
                return "e8";
            case f8:
                return "f8";
            case g8:
                return "g8";
            case h8:
                return "h8";
            default:
                return "--";
        }
    }

    Square getSquareIndex(const std::string& s)
    {
        if (s == "a1" || s == "A1")
        {
            return a1;
        }
        if (s == "b1" || s == "B1")
        {
            return b1;
        }
        if (s == "c1" || s == "C1")
        {
            return c1;
        }
        if (s == "d1" || s == "D1")
        {
            return d1;
        }
        if (s == "e1" || s == "E1")
        {
            return e1;
        }
        if (s == "f1" || s == "F1")
        {
            return f1;
        }
        if (s == "g1" || s == "G1")
        {
            return g1;
        }
        if (s == "h1" || s == "H1")
        {
            return h1;
        }
        if (s == "a2" || s == "A2")
        {
            return a2;
        }
        if (s == "b2" || s == "B2")
        {
            return b2;
        }
        if (s == "c2" || s == "C2")
        {
            return c2;
        }
        if (s == "d2" || s == "D2")
        {
            return d2;
        }
        if (s == "e2" || s == "E2")
        {
            return e2;
        }
        if (s == "f2" || s == "F2")
        {
            return f2;
        }
        if (s == "g2" || s == "G2")
        {
            return g2;
        }
        if (s == "h2" || s == "H2")
        {
            return h2;
        }
        if (s == "a3" || s == "A3")
        {
            return a3;
        }
        if (s == "b3" || s == "B3")
        {
            return b3;
        }
        if (s == "c3" || s == "C3")
        {
            return c3;
        }
        if (s == "d3" || s == "D3")
        {
            return d3;
        }
        if (s == "e3" || s == "E3")
        {
            return e3;
        }
        if (s == "f3" || s == "F3")
        {
            return f3;
        }
        if (s == "g3" || s == "G3")
        {
            return g3;
        }
        if (s == "h3" || s == "H3")
        {
            return h3;
        }
        if (s == "a4" || s == "A4")
        {
            return a4;
        }
        if (s == "b4" || s == "B4")
        {
            return b4;
        }
        if (s == "c4" || s == "C4")
        {
            return c4;
        }
        if (s == "d4" || s == "D4")
        {
            return d4;
        }
        if (s == "e4" || s == "E4")
        {
            return e4;
        }
        if (s == "f4" || s == "F4")
        {
            return f4;
        }
        if (s == "g4" || s == "G4")
        {
            return g4;
        }
        if (s == "h4" || s == "H4")
        {
            return h4;
        }
        if (s == "a5" || s == "A5")
        {
            return a5;
        }
        if (s == "b5" || s == "B5")
        {
            return b5;
        }
        if (s == "c5" || s == "C5")
        {
            return c5;
        }
        if (s == "d5" || s == "D5")
        {
            return d5;
        }
        if (s == "e5" || s == "E5")
        {
            return e5;
        }
        if (s == "f5" || s == "F5")
        {
            return f5;
        }
        if (s == "g5" || s == "G5")
        {
            return g5;
        }
        if (s == "h5" || s == "H5")
        {
            return h5;
        }
        if (s == "a6" || s == "A6")
        {
            return a6;
        }
        if (s == "b6" || s == "B6")
        {
            return b6;
        }
        if (s == "c6" || s == "C6")
        {
            return c6;
        }
        if (s == "d6" || s == "D6")
        {
            return d6;
        }
        if (s == "e6" || s == "E6")
        {
            return e6;
        }
        if (s == "f6" || s == "F6")
        {
            return f6;
        }
        if (s == "g6" || s == "G6")
        {
            return g6;
        }
        if (s == "h6" || s == "H6")
        {
            return h6;
        }
        if (s == "a7" || s == "A7")
        {
            return a7;
        }
        if (s == "b7" || s == "B7")
        {
            return b7;
        }
        if (s == "c7" || s == "C7")
        {
            return c7;
        }
        if (s == "d7" || s == "D7")
        {
            return d7;
        }
        if (s == "e7" || s == "E7")
        {
            return e7;
        }
        if (s == "f7" || s == "F7")
        {
            return f7;
        }
        if (s == "g7" || s == "G7")
        {
            return g7;
        }
        if (s == "h7" || s == "H7")
        {
            return h7;
        }
        if (s == "a8" || s == "A8")
        {
            return a8;
        }
        if (s == "b8" || s == "B8")
        {
            return b8;
        }
        if (s == "c8" || s == "C8")
        {
            return c8;
        }
        if (s == "d8" || s == "D8")
        {
            return d8;
        }
        if (s == "e8" || s == "E8")
        {
            return e8;
        }
        if (s == "f8" || s == "F8")
        {
            return f8;
        }
        if (s == "g8" || s == "G8")
        {
            return g8;
        }
        if (s == "h8" || s == "H8")
        {
            return h8;
        }
        return no_square;
    }

    bool isMove(const std::string move_string)
    {
        if (
            move_string.size() >= 4 &&
            (move_string[0] == 'a' ||
             move_string[0] == 'b' ||
             move_string[0] == 'c' ||
             move_string[0] == 'd' ||
             move_string[0] == 'e' ||
             move_string[0] == 'f' ||
             move_string[0] == 'g' ||
             move_string[0] == 'h') &&
            (move_string[1] == '1' ||
             move_string[1] == '2' ||
             move_string[1] == '3' ||
             move_string[1] == '4' ||
             move_string[1] == '5' ||
             move_string[1] == '6' ||
             move_string[1] == '7' ||
             move_string[1] == '8') &&
            (move_string[2] == 'a' ||
             move_string[2] == 'b' ||
             move_string[2] == 'c' ||
             move_string[2] == 'd' ||
             move_string[2] == 'e' ||
             move_string[2] == 'f' ||
             move_string[2] == 'g' ||
             move_string[2] == 'h') &&
            (move_string[3] == '1' ||
             move_string[3] == '2' ||
             move_string[3] == '3' ||
             move_string[3] == '4' ||
             move_string[3] == '5' ||
             move_string[3] == '6' ||
             move_string[3] == '7' ||
             move_string[3] == '8') &&
            (move_string.size() == 4 ||
             move_string[4] == 'k' ||
             move_string[4] == 'b' ||
             move_string[4] == 'r' ||
             move_string[4] == 'q')
            )
        {
            return true;
        }
        return false;
    }


    Move getMove(const std::string move_string, const Position position)
    {
        Player us = position.us;

        Square from = no_square;
        Square to = no_square;
        Piece moved = no_piece;
        Piece captured = no_piece;
        Piece promoted = no_piece;
        bool captured_enpassant = false;
        bool castled = false;
        Square enpassant = no_square;


        from = getSquareIndex(std::string(move_string, 0, 2));
        to = getSquareIndex(std::string(move_string, 2, 2));
        if (move_string.size() == 5)
        {
            promoted = getPiece(std::string(move_string, 4, 1)).piece;
        }
        for (Piece i = 0; i < no_piece; ++i)
        {
            if ((position.pieces[i] & bitboard::bitAtIndex(from)) != 0)
            {
                moved = i;
            }
            if ((position.pieces[i] & bitboard::bitAtIndex(to)) != 0)
            {
                captured = i;
            }
        }
        //captured en passant
        if (
            moved == pawn &&
                (bitboard::bitAtIndex(to) & (position.enpassant_castling & (bitboard::rank(a3) | bitboard::rank(a6)))) != 0
            )
        {
            captured_enpassant = true;
        }
        //pawn double push
        if (
            moved == pawn &&
            (bitboard::bitAtIndex(from) & bitboard::pawnHomeRank(us)) != 0 &&
                (bitboard::bitAtIndex(to) & (bitboard::attackMaskPawnQuiet(us, from) | bitboard::attackMaskPawnCapture(us, from))) == 0
            )
        {
            enpassant = Square(bitboard::ctz(attackMaskPawnQuiet(us, from)));
        }
        //castling
        //queenside
        if (
            from == castling::king_from_square[us] &&
            to == castling::queenside_king_to_square[us] &&
            (position.enpassant_castling & castling::queenside_rook_from[us]) != 0 &&
            (position.enpassant_castling & castling::king_from[us]) != 0
            )
        {
            castled = true;
        }
        //kingside
        if (
            from == castling::king_from_square[us] &&
            to == castling::kingside_king_to_square[us] &&
            (position.enpassant_castling & castling::kingside_rook_from[us]) != 0 &&
            (position.enpassant_castling & castling::king_from[us]) != 0
            )
        {
            castled = true;
        }

        Move move;
        move.set(from, to, moved, captured, promoted, castled, captured_enpassant, enpassant);
        return move;
    }

    std::string getString(const uint64_t& word)
    {
        std::string ret = " _ _ _ _ _ _ _ _\n";
        for (size_t rank = 7; rank < 8; rank--)
        {
            for (size_t file = 0; file < 8; file++)
            {
                if ((bitboard::bitAtIndex(8 * rank + file) & word) != 0)
                {
                    ret += "|" + black_circle_unicode;
                }
                else
                {
                    ret += "|_";
                }
            }
            ret += "|" + std::to_string(rank + 1) + "\n";
        }
        ret += " A B C D E F G H";
        return ret;
    }

    std::string getString(std::vector<Move> moves)
    {
        std::string ret = "";
        for (auto move : moves)
        {
            ret += move.getNotation() + " ";
        }
        return ret;
    }

    void setIn(long milliseconds, std::atomic<bool>& set)
    {
        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        long time_elapsed;
        do
        {
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        } while (time_elapsed <= milliseconds && !set);

        set = true;
    }

    std::vector<std::string> split(const std::string& s, char delim)
    {
        std::vector<std::string> elements;
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim))
        {
            if (item != "")
            {
                elements.push_back(item);
            }
        }
        return elements;
    }

    EndOfGame gameEnded(const Position& position, const std::vector<Position>& history)
    {
        bool noLegalMoves = true;

        Move moves[max_num_quiets + max_num_captures];
        uint8_t numMoves = position.generateMoves(moves);
        for (uint8_t i = 0; i < numMoves; i++)
        {
            if (position.isLegal(moves[i]))
            {
                noLegalMoves = false;
                break;
            }
        }

        if (position.inCheck(position.us, position.enemy) && noLegalMoves)
        {
            return win[position.enemy];
        }
        else if (noLegalMoves)
        {
            return EndOfGame::draw;
        }

        if (position.halfmove_clock >= 100)
        {
            return EndOfGame::draw;
        }

        int positionCounter = 0;
        for (auto& p : history)
        {
            if (p.zobrist_key == position.zobrist_key)
            {
                positionCounter += 1;
            }
        }
        if (positionCounter >= 3)
        {
            return EndOfGame::draw;
        }

        return EndOfGame::not_ended;
    }
}
