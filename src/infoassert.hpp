#pragma once

#include <iostream>

#ifndef NDEBUG
#define infoassert(_statement) \
    {\
        if(not (_statement))\
        {\
            std::cout << "\nAborting:" << std::endl;\
            std::cout << "info In file " << __FILE__ << ", line " <<  __LINE__ << ": Assertion '" << #_statement << "' failed." << std::endl;\
            exit(-1);\
        }\
    }
#else
#define infoassert(statement)
#endif
