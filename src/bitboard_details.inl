namespace details
{
    constexpr std::array<uint64_t, 64> bit_at_index = []()
    {
        std::array<uint64_t, 64> bit_at_index_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            bit_at_index_ret[i] = (uint64_t)(0b1) << i;
        }
        return bit_at_index_ret;
    }();
    constexpr std::array<uint64_t, 64> ranks_64 = []()
    {
        uint64_t ranks[8] = {};
        for (size_t i = 0; i < 8; i++)
        {
            ranks[i] = (uint64_t)(0b11111111) << (i * 8);
        }
        std::array<uint64_t, 64> ranks_64_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            ranks_64_ret[i] = ranks[i / 8];
        }
        return ranks_64_ret;
    }();
    constexpr std::array<uint64_t, 64> files_64 = []()
    {
        uint64_t files[8] = {};
        for (size_t i = 0; i < 8; i++)
        {
            files[i] = (uint64_t)(0b0000000100000001000000010000000100000001000000010000000100000001) << i;
        }
        std::array<uint64_t, 64> files_64_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            files_64_ret[i] = files[i % 8];
        }
        return files_64_ret;
    }();
    constexpr uint64_t upper_left_side_zero =  0b1000000011000000111000001111000011111000111111001111111011111111;
    constexpr uint64_t lower_right_side_zero = 0b1111111101111111001111110001111100001111000001110000001100000001;
    constexpr uint64_t lower_left_side_zero = 0b1111111011111100111110001111000011100000110000001000000000000000;
    constexpr uint64_t upper_right_side_zero = 0b0000000000000001000000110000011100001111000111110011111101111111;
    constexpr uint64_t main_diagonal = 0b1000000001000000001000000001000000001000000001000000001000000001; //A1 to H8
    constexpr uint64_t anti_diagonal = 0b0000000100000010000001000000100000010000001000000100000010000000; //H1 to A8
    constexpr std::array<uint64_t, 64> diagonals_64 = []()
    {
        std::array<uint64_t, 64> diagonals_64_ret = {};
        for (size_t i = 0; i < 8; i++)
        {
            uint64_t current_diagonal = (main_diagonal << i) & upper_left_side_zero;
            uint64_t tmp = current_diagonal;
            while (tmp != 0)
            {
                size_t index = ctz(tmp);
                diagonals_64_ret[index] = current_diagonal;
                tmp = tmp & (~bit_at_index[index]);
            }
        }
        for (size_t i = 1; i < 8; i++)
        {
            uint64_t current_diagonal = (main_diagonal >> i) & lower_right_side_zero;
            uint64_t tmp = current_diagonal;
            while (tmp != 0)
            {
                size_t index = ctz(tmp);
                diagonals_64_ret[index] = current_diagonal;
                tmp = tmp & ~bit_at_index[index];
            }
        }
        return diagonals_64_ret;
    }();
    constexpr std::array<uint64_t, 64> anti_diagonals_64 = []()
    {
        std::array<uint64_t, 64> anti_diagonals_64_ret = {};
        for (int i = 1; i < 8; i++)
        {
            uint64_t current_anti_diagonal = (anti_diagonal << i) & lower_left_side_zero;
            uint64_t tmp = current_anti_diagonal;
            while (tmp != 0)
            {
                size_t index = ctz(tmp);
                anti_diagonals_64_ret[index] = current_anti_diagonal;
                tmp = tmp & ~bit_at_index[index];
            }
        }
        {
            uint64_t current_anti_diagonal = anti_diagonal;
            uint64_t tmp = current_anti_diagonal;
            while (tmp != 0)
            {
                size_t index = ctz(tmp);
                anti_diagonals_64_ret[index] = current_anti_diagonal;
                tmp = tmp & ~bit_at_index[index];
            }
        }
        for (int i = 1; i < 8; i++)
        {
            uint64_t current_anti_diagonal = (anti_diagonal >> i) & upper_right_side_zero;
            uint64_t tmp = current_anti_diagonal;
            while (tmp != 0)
            {
                size_t index = ctz(tmp);
                anti_diagonals_64_ret[index] = current_anti_diagonal;
                tmp = tmp & ~bit_at_index[index];
            }
        }
        return anti_diagonals_64_ret;
    }();

    constexpr inline uint8_t hashkeyRank(const Square& square, const uint64_t& occupancy)
    {
        return ((occupancy >> ((square / 8) * 8)) >> 1) & 0b111111U;
    }
    constexpr inline uint8_t hashkeyFile(const Square& square, const uint64_t& occupancy)
    {
        return (((((occupancy >> (square % 8)) & details::files_64[0]) * details::main_diagonal) >> 56) >> 1) & 0b111111U;
    }
    constexpr inline uint8_t hashkeyDiagonal(const Square& square, const uint64_t& occupancy)
    {
        return ((((occupancy & details::diagonals_64[square]) * details::files_64[0]) >> 56) >> 1) & 0b111111U;
    }
    constexpr inline uint8_t hashkeyAntiDiagonal(const Square& square, const uint64_t& occupancy)
    {
        return ((((occupancy & details::anti_diagonals_64[square]) * details::files_64[0]) >> 56) >> 1) & 0b111111U;
    }

    constexpr ssize_t north = 8;
    constexpr ssize_t south = -8;
    constexpr ssize_t east = 1;
    constexpr ssize_t west = -1;
    constexpr ssize_t north_east = 9;
    constexpr ssize_t north_west = 7;
    constexpr ssize_t south_west = -9;
    constexpr ssize_t south_east = -7;
    constexpr std::array<uint64_t, 64> possible_ranks = []()
    {
        std::array<uint64_t, 64> possible_ranks_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            uint64_t tmp = (uint64_t)(0b10000001) | (((uint64_t)(i)) << 1);
            for (int j = 0; j < 8; j++)
            {
                possible_ranks_ret[i] |= tmp << (j * 8);
            }
        }
        return possible_ranks_ret;
    }();
    constexpr std::array<uint64_t, 64> possible_files = []()
    {
        std::array<uint64_t, 64> possible_files_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            auto rankToFile = [&](uint64_t rank)
            {
                return (((rank & 0b11111111U) * main_diagonal) & files_64[7]) >> 7;
            };
            uint64_t tmp = rankToFile((uint64_t)(0b10000001) | (((uint64_t)(i)) << 1));
            for (int j = 0; j < 8; j++)
            {
                possible_files_ret[i] |= tmp << j;
            }
        }
        return possible_files_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 64> rank_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 64> rank_attack_table_ret = {};
        for (size_t index = 0; index < 64; index++)
        {
            for (size_t possible_ranks_index = 0; possible_ranks_index < 64; possible_ranks_index++)
            {
                uint64_t tmp_attack_mask = 0;
                uint64_t occupancy = possible_ranks[possible_ranks_index];
                size_t i = index;
                while (true)
                {
                    i += east;
                    if (i >= 64)
                    {
                        break;
                    }
                    if (index % 8 == 7)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                i = index;
                while (true)
                {
                    i += west;
                    if (i >= 64) //equals i < 0)
                    {
                        break;
                    }
                    if (index % 8 == 0)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                rank_attack_table_ret[index][hashkeyRank(index, occupancy)] = tmp_attack_mask;
            }
        }
        return rank_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 64> file_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 64> file_attack_table_ret = {};
        for (size_t index = 0; index < 64; index++)
        {
            for (size_t possible_files_index = 0; possible_files_index < 64; possible_files_index++)
            {
                uint64_t tmp_attack_mask = 0;
                uint64_t occupancy = possible_files[possible_files_index];
                size_t i = index;
                while (true)
                {
                    i += north;
                    if (i >= 64)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                i = index;
                while (true)
                {
                    i += south;
                    if (i >= 64/*equals i < 0*/)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                file_attack_table_ret[index][hashkeyFile(index, occupancy)] = tmp_attack_mask;
            }
        }
        return file_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 64> diagonal_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 64> diagonal_attack_table_ret = {};
        for (size_t index = 0; index < 64; index++)
        {
            for (size_t possible_ranks_index = 0; possible_ranks_index < 64; possible_ranks_index++)
            {
                uint64_t tmp_attack_mask = 0;
                uint64_t occupancy = possible_ranks[possible_ranks_index];
                size_t i = index;
                while (true)
                {
                    i += north_east;
                    if (i >= 64)
                    {
                        break;
                    }
                    if (index % 8 == 7)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                i = index;
                while (true)
                {
                    i += south_west;

                    if (i >= 64/*equals i < 0*/)
                    {
                        break;
                    }
                    if (index % 8 == 0)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                diagonal_attack_table_ret[index][hashkeyDiagonal(index, occupancy)] = tmp_attack_mask;
            }
        }
        return diagonal_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 64> anti_diagonal_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 64> anti_diagonal_attack_table_ret = {};
        for (size_t index = 0; index < 64; index++)
        {
            for (size_t possible_ranks_index = 0; possible_ranks_index < 64; possible_ranks_index++)
            {
                uint64_t tmp_attack_mask = 0;
                uint64_t occupancy = possible_ranks[possible_ranks_index];
                size_t i = index;
                while (true)
                {
                    i += north_west;
                    if (i >= 64)
                    {
                        break;
                    }
                    if (index % 8 == 0)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                i = index;
                while (true)
                {
                    i += south_east;
                    if (i >= 64/*equals i < 0*/)
                    {
                        break;
                    }
                    if (index % 8 == 7)
                    {
                        break;
                    }
                    if ((occupancy & bit_at_index[i]) != 0)
                    {
                        tmp_attack_mask |= bit_at_index[i];
                        break;
                    }
                    else
                    {
                        tmp_attack_mask |= bit_at_index[i];
                    }
                }
                anti_diagonal_attack_table_ret[index][hashkeyAntiDiagonal(index, occupancy)] = tmp_attack_mask;
            }
        }
        return anti_diagonal_attack_table_ret;
    }();
    constexpr std::array<uint64_t, 64> knight_attack_table = []()
    {
        std::array<uint64_t, 64> knight_attack_table_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            knight_attack_table_ret[i] = 0;
            if (i + north + north_west < 64 && i % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + north + north_west];
            }
            if (i + north + north_east < 64 && (i + 1) % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + north + north_east];
            }
            if (i + north_west + west < 64 && i % 8 > 1)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + north_west + west];
            }
            if (i + north_east + east < 64 && (i + 1) % 8 < 7 && (i + 1) % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + north_east + east];
            }
            if (i + south + south_west < 64 && i % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + south + south_west];
            }
            if (i + south + south_east < 64 && (i + 1) % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + south + south_east];
            }
            if (i + south_west + west < 64 && i % 8 > 1)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + south_west + west];
            }
            if (i + south_east + east < 64 && (i + 1) % 8 < 7 && (i + 1) % 8 != 0)
            {
                knight_attack_table_ret[i] |= bit_at_index[i + south_east + east];
            }
        }
        return knight_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 6> nxn_mask = []()
    {
        std::array<std::array<uint64_t, 64>, 6> ret = {};
        for(int mask_size = 1; mask_size<6; ++mask_size)
        {
            for(Square square = 0; square<no_square; ++square)
            {
                for(int current_y_offset = -mask_size+1; current_y_offset<mask_size; ++current_y_offset)
                {
                    for(int current_x_offset = -mask_size+1; current_x_offset<mask_size; ++current_x_offset)
                    {
                        if(square%8 + current_x_offset < 0 || square%8 + current_x_offset >= 8)
                        {
                            continue;
                        }
                        const Square current_square = square + 8*current_y_offset + current_x_offset;
                        if(current_square < no_square)
                        {
                            ret[mask_size][square] |= bit_at_index[current_square];
                        }
                    }
                }

            }
        }
        return ret;
    }();
    constexpr std::array<uint64_t, 64> king_attack_table = []()
    {
        std::array<uint64_t, 64> king_attack_table_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            king_attack_table_ret[i] = nxn_mask[2][i];
        }
        return king_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 2> pawn_quiet_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 2> pawn_quiet_attack_table_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            //white
            if (i + north < 64)
            {
                pawn_quiet_attack_table_ret[0][i] |= bit_at_index[i + north];
            }
            //black
            if (i + south < 64)
            {
                pawn_quiet_attack_table_ret[1][i] |= bit_at_index[i + south];
            }
        }
        return pawn_quiet_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 2> pawn_capture_attack_table = []()
    {
        std::array<std::array<uint64_t, 64>, 2> pawn_capture_attack_table_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            //white
            if (i + north < 64)
            {
                if (i % 8 != 0)
                {
                    pawn_capture_attack_table_ret[0][i] |= bit_at_index[i + north_west];
                }
                if (i % 8 != 7)
                {
                    pawn_capture_attack_table_ret[0][i] |= bit_at_index[i + north_east];
                }
            }
            //black
            if (i + south < 64)
            {
                if (i % 8 != 0)
                {
                    pawn_capture_attack_table_ret[1][i] |= bit_at_index[i + south_west];
                }
                if (i % 8 != 7)
                {
                    pawn_capture_attack_table_ret[1][i] |= bit_at_index[i + south_east];
                }
            }
        }
        return pawn_capture_attack_table_ret;
    }();
    constexpr std::array<std::array<uint64_t, 64>, 2> is_passed = []()
    {
        std::array<std::array<uint64_t, 64>, 2> is_passed_ret = {};
        for (size_t i = 0; i < 64; i++)
        {
            is_passed_ret[0][i] |= files_64[i];
            if (i % 8 != 0)
            {
                is_passed_ret[0][i] |= files_64[i - 1];
            }
            if (i % 8 != 7)
            {
                is_passed_ret[0][i] |= files_64[i + 1];
            }
            is_passed_ret[1][i] = is_passed_ret[0][i];

            //WHITE
            for (size_t j = 0; j < (i / 8) + 1; j++)
            {
                is_passed_ret[0][i] &= ~ranks_64[j];
            }
            //BLACK
            for (size_t j = (i / 8); j < 8; j++)
            {
                is_passed_ret[1][i] &= ~ranks_64[j];
            }
        }
        return is_passed_ret;
    }();

    constexpr uint64_t zobrist_random_bitmasks_pieces[6][64] =
#include "data/zobrist_random_bitmasks_pieces.inl"
;
constexpr uint64_t zobrist_random_bitmasks_players[2][64] =
#include "data/zobrist_random_bitmasks_players.inl"
    ;
    constexpr uint64_t zobrist_random_bitmasks_side_to_move[2] = {6436801082468244238U, 18024204846139045185U};

    constexpr uint64_t home_rank[2] = {ranks_64[a1], ranks_64[a8]};
    constexpr uint64_t pawn_home_rank[2] = {ranks_64[a2], ranks_64[a7]};
}
