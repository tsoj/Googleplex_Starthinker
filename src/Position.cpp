#include "Position.hpp"
#include "bitboard.hpp"
#include "castling.hpp"
#include "util.hpp"
#include "movegen.hpp"
#include "infoassert.hpp"

#include <sstream>
#include <iostream>
#include <algorithm>

namespace
{
    constexpr Position empty_position = Position
        {
            {0, 0, 0, 0, 0, 0},
            {0, 0},
            0,
            0,
            no_player,
            no_player,
            0,
            0,
        };
}

Position Position::fen(const std::string& fen)
{
    Position position = empty_position;
    std::stringstream buffer = std::stringstream(fen);
    std::string pieces;
    std::getline(buffer, pieces, ' ');
    std::string active_color;
    std::getline(buffer, active_color, ' ');
    std::string castling_availability;
    std::getline(buffer, castling_availability, ' ');
    std::string enpassant_target_square;
    std::getline(buffer, enpassant_target_square, ' ');
    std::string halfmove_clock;
    std::getline(buffer, halfmove_clock, ' ');
    std::string fullmove_number;
    std::getline(buffer, fullmove_number, ' ');

    Square square_counter = 56;
    for (auto i : pieces)
    {
        switch (i)
        {
            case '/':
            {
                square_counter -= 16;
                break;
            }
            case '8':
            {
                square_counter += 8;
                break;
            }
            case '7':
            {
                square_counter += 7;
                break;
            }
            case '6':
            {
                square_counter += 6;
                break;
            }
            case '5':
            {
                square_counter += 5;
                break;
            }
            case '4':
            {
                square_counter += 4;
                break;
            }
            case '3':
            {
                square_counter += 3;
                break;
            }
            case '2':
            {
                square_counter += 2;
                break;
            }
            case '1':
            {
                square_counter += 1;
                break;
            }
            case '0':
            {
                square_counter += 0;
                break;
            }
            case 'P':
            {
                position.addPiece(white, pawn, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'N':
            {
                position.addPiece(white, knight, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'B':
            {
                position.addPiece(white, bishop, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'R':
            {
                position.addPiece(white, rook, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'Q':
            {
                position.addPiece(white, queen, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'K':
            {
                position.addPiece(white, king, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'p':
            {
                position.addPiece(black, pawn, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'n':
            {
                position.addPiece(black, knight, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'b':
            {
                position.addPiece(black, bishop, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'r':
            {
                position.addPiece(black, rook, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'q':
            {
                position.addPiece(black, queen, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            case 'k':
            {
                position.addPiece(black, king, bitboard::bitAtIndex(square_counter));
                square_counter += 1;
                break;
            };
            default:
                return empty_position;
        }
    }
    if (active_color == "w" || active_color == "W")
    {
        position.us = white;
        position.enemy = black;
    }
    else if (active_color == "b" || active_color == "B")
    {
        position.us = black;
        position.enemy = white;
    }
    else
    {
        return empty_position;
    }
    for (auto i : castling_availability)
    {
        switch (i)
        {
            case '-':
            {
                break;
            }
            case 'K':
            {
                position.enpassant_castling |= castling::kingside_rook_from[white] | castling::king_from[white];
                break;
            }
            case 'k':
            {
                position.enpassant_castling |= castling::kingside_rook_from[black] | castling::king_from[black];
                break;
            }
            case 'Q':
            {
                position.enpassant_castling |= castling::queenside_rook_from[white] | castling::king_from[white];
                break;
            }
            case 'q':
            {
                position.enpassant_castling |= castling::queenside_rook_from[black] | castling::king_from[black];
                break;
            }
            default:
                return empty_position;
        }
    }
    if (enpassant_target_square != "-")
    {
        Square enpassant_target_square_index = util::getSquareIndex(enpassant_target_square);
        if (enpassant_target_square_index >= 64)
        {
            return empty_position;
        }
        position.enpassant_castling |= bitboard::bitAtIndex(enpassant_target_square_index);
    }
    position.halfmove_clock = std::stoi(halfmove_clock);
    position.fullmoves_played = std::stoi(fullmove_number);
    position.zobrist_key = position.getZobristKey();
    return position;
}

std::string Position::getFen() const
{
    std::string fen = "";

    int empty_square_counter = 0;
    for (ssize_t rank = 7; rank >= 0; rank--)
    {
        for (size_t file = 0; file < 8; file++)
        {
            auto[piece, player] = getPiece(rank * 8 + file);
            if (player != no_player)
            {
                if (empty_square_counter > 0)
                {
                    fen += std::to_string(empty_square_counter);
                    empty_square_counter = 0;
                }
                fen += util::getPieceNotation(player, piece);
            }
            else
            {
                empty_square_counter += 1;
            }

        }
        if (empty_square_counter > 0)
        {
            fen += std::to_string(empty_square_counter);
            empty_square_counter = 0;
        }
        if (rank != 0)
        {
            fen += "/";
        }
    }

    fen += " ";

    fen += us == white ? "w" : "b";

    fen += " ";

    if ((enpassant_castling & castling::king_from[white]) != 0)
    {
        if ((enpassant_castling & castling::kingside_rook_from[white]) != 0)
        {
            fen += "K";
        }
        if ((enpassant_castling & castling::queenside_rook_from[white]) != 0)
        {
            fen += "Q";
        }
    }
    if ((enpassant_castling & castling::king_from[white]) != 0)
    {
        if ((enpassant_castling & castling::kingside_rook_from[black]) != 0)
        {
            fen += "k";
        }
        if ((enpassant_castling & castling::queenside_rook_from[black]) != 0)
        {
            fen += "q";
        }
    }

    if (fen.back() == ' ')
    {
        fen += "-";
    }

    fen += " ";

    if ((enpassant_castling & (bitboard::rank(a3) | bitboard::rank(a6))) != 0)
    {
        Square enpassant = bitboard::ctz((enpassant_castling & (bitboard::rank(a3) | bitboard::rank(a6))));
        fen += util::getSquareNotation(enpassant);
    }
    else
    {
        fen += "-";
    }

    fen += " ";

    fen += std::to_string(halfmove_clock);

    fen += " ";

    fen += std::to_string(fullmoves_played);

    return fen;
}

std::string Position::getString() const
{
    std::string ret = " _ _ _ _ _ _ _ _\n";
    for (size_t rank = 7; rank < 8; rank--)
    {
        for (size_t file = 0; file < 8; file++)
        {
            bool foundPiece = false;
            auto[piece, player] = getPiece(8 * rank + file);
            if (player != no_player)
            {
                ret += "|" + util::getUnicodePiece(player, piece);
                foundPiece = true;
            }
            if (foundPiece == false)
            {
                ret += "|_";
            }
        }
        ret += "|" + std::to_string(rank + 1) + "\n";
    }
    ret += " A B C D E F G H";
    return ret;
}

std::string Position::debugString() const
{
    std::string ret = "";
    ret += "whose move: ";
    ret += std::to_string(us);
    ret += "\n-----------------------\n";
    ret += "fullmoves played: ";
    ret += std::to_string(fullmoves_played);
    ret += "\n-----------------------\n";
    ret += "halfmove clock: ";
    ret += std::to_string(halfmove_clock);
    ret += "\n-----------------------\n";
    ret += "zobrist key: ";
    ret += std::to_string(zobrist_key);
    ret += "\n-----------------------\n";
    ret += "castling / en passant\n";
    ret += util::getString(enpassant_castling);
    ret += "\n-----------------------\n";
    ret += "white:\n";
    ret += util::getString(players[white]);
    ret += "\n-----------------------\n";
    ret += "black:\n";
    ret += util::getString(players[black]);
    ret += "\n-----------------------\n";
    ret += "pawns:\n";
    ret += util::getString(pieces[pawn]);
    ret += "\n-----------------------\n";
    ret += "knights:\n";
    ret += util::getString(pieces[knight]);
    ret += "\n-----------------------\n";
    ret += "bishops:\n";
    ret += util::getString(pieces[bishop]);
    ret += "\n-----------------------\n";
    ret += "rooks:\n";
    ret += util::getString(pieces[rook]);
    ret += "\n-----------------------\n";
    ret += "queens:\n";
    ret += util::getString(pieces[queen]);
    ret += "\n-----------------------\n";
    ret += "kings:\n";
    ret += util::getString(pieces[king]);
    ret += "\n-----------------------\n";
    return ret;
}

std::string Position::peudoLegalMoves() const
{
    std::string ret = "";
    Move moves[max_num_quiets + max_num_captures];
    uint8_t num_moves = this->generateMoves(moves);
    for (uint8_t i = 0; i < num_moves; i++)
    {
        Position pos = *this;
        pos.doMove(moves[i]);
        ret += pos.getString();
    }
    ret += std::to_string((int) num_moves);
    return ret;
}

std::pair<Piece, Player> Position::getPiece(const Square square) const
{
    struct Ret
    {
        Piece piece;
        Player player;
    };
    for (Player player = 0; player < no_player; player += 1)
    {
        for (Piece piece = 0; piece < no_piece; piece += 1)
        {
            if ((bitboard::bitAtIndex(square) & this->pieces[piece] & this->players[player]) != 0)
            {
                return std::make_pair(piece, player);
            }
        }
    }
    return std::make_pair(no_piece, no_player);
}

void Position::addPiece(const Player player, const Piece piece, const uint64_t to)
{
    this->pieces[piece] |= to;
    this->players[player] |= to;
}

void Position::removePiece(const Player player, const Piece piece, const uint64_t from)
{
    this->pieces[piece] &= ~from;
    this->players[player] &= ~from;
}

void Position::movePiece(const Player player, const Piece piece, const uint64_t from, const uint64_t to)
{
    this->removePiece(player, piece, from);
    this->addPiece(player, piece, to);
}

uint64_t Position::occupancy() const
{
    return players[white] | players[black];
}

uint64_t Position::attackers(const Player us, const Player enemy, const Square attacked_square) const
{
    const uint64_t occupancy = this->players[white] | this->players[black];
    return
        (((bitboard::attackMask(queen, attacked_square, occupancy) & this->pieces[queen]) |
          (bitboard::attackMask(knight, attacked_square, occupancy) & this->pieces[knight]) |
          (bitboard::attackMask(bishop, attacked_square, occupancy) & this->pieces[bishop]) |
          (bitboard::attackMask(rook, attacked_square, occupancy) & this->pieces[rook]) |
          (bitboard::attackMask(king, attacked_square, occupancy) & this->pieces[king]) |
          (bitboard::attackMaskPawnCapture(us, attacked_square) & this->pieces[pawn])) & this->players[enemy]);
}

bool Position::attacked(const Player us, const Player enemy, const Square attacked_square) const
{
    bool ret = this->attackers(us, enemy, attacked_square);
    return ret;
}

bool Position::inCheck(const Player us, const Player enemy) const
{
    const Square kings_square = bitboard::ctz(this->pieces[king] & this->players[us]);
    return this->attacked(us, enemy, kings_square);
}

bool Position::isPassedPawn(const Player us, const Player enemy, const Square square) const
{
    return (bitboard::isPassed(us, square) & pieces[pawn] & players[enemy]) == 0;
}

bool Position::isPseudoLegal(const Move move) const
{
    const Square to = move.to();
    const uint64_t to_BB = bitboard::bitAtIndex(to);
    const Square from = move.from();
    const uint64_t from_BB = bitboard::bitAtIndex(from);
    const Piece moved = move.moved();
    const Piece captured = move.captured();
    const Square enpassant = move.enpassant();
    const bool captured_enpassant = move.captured_enpassant();
    const uint64_t occupancy = this->players[white] | this->players[black];

    if ((from_BB & this->players[us] & this->pieces[moved]) == 0)
    {
        return false;
    }

    if ((to_BB & this->players[us]) != 0)
    {
        return false;
    }

    if (captured != no_piece && (to_BB & this->players[enemy] & this->pieces[captured]) == 0 && !captured_enpassant)
    {
        return false;
    }

    if (captured == no_piece && (to_BB & this->players[enemy]) != 0)
    {
        return false;
    }

    if (moved == pawn && captured == no_piece &&
        ((occupancy & to_BB) != 0 || (enpassant != no_square && ((bitboard::bitAtIndex(enpassant) & occupancy) != 0))))
    {
        return false;
    }

    if (captured_enpassant && (to_BB & this->enpassant_castling & ~(bitboard::rank(a1) | bitboard::rank(a8))) == 0)
    {
        return false;
    }

    if (move.castled())
    {
        if ((this->enpassant_castling & castling::king_from[this->us]) == 0)
        {
            return false;
        }
        // queenside
        if (to == castling::queenside_king_to_square[this->us])
        {
            if (
                (this->enpassant_castling & castling::queenside_rook_from[this->us]) == 0 ||
                (castling::queenside_block_relevant_area[us] & occupancy) != 0 ||
                this->attacked(this->us, this->enemy, castling::queenside_check_relevant_squares[this->us][0]) ||
                this->attacked(this->us, this->enemy, castling::queenside_check_relevant_squares[this->us][1])
                )
            {
                return false;
            }
        }
            // kingside
        else
        {
            if (
                (this->enpassant_castling & castling::kingside_rook_from[this->us]) == 0 ||
                (castling::kingside_block_relevant_area[this->us] & occupancy) != 0 ||
                this->attacked(this->us, this->enemy, castling::kingside_check_relevant_squares[this->us][0]) ||
                this->attacked(this->us, this->enemy, castling::kingside_check_relevant_squares[this->us][1])
                )
            {
                return false;
            }
        }
    }
    if (moved == bishop || moved == rook || moved == queen)
    {
        uint64_t attack_mask = bitboard::attackMask(moved, from, occupancy);
        if ((to_BB & attack_mask) == 0)
        {
            return false;
        }
    }


    return true;
}

bool Position::isLegal(const Move move) const
{
    Position new_position = *this;
    new_position.doMove(move);
    return isPseudoLegal(move) && !new_position.inCheck(us, enemy);
}

bool Position::insufficientMaterial() const
{
    // at most one minor piece
    if (
        (pieces[pawn] | pieces[rook] | pieces[queen]) == 0 &&
        bitboard::popcount(pieces[bishop] | pieces[knight]) <= 1
        )
    {
        return true;
    }

    return false;
}

Phase Position::getPhase() const
{
    const int8_t material = bitboard::popcount(players[white] | players[black]);
    return std::clamp((Phase) ((material * 32) / 13 - 18), endgame, opening);
}

Score Position::see(const Square attacked_square, Piece attacked_piece) const
{
    Position new_position = *this;

    const Player return_us = new_position.us;
    const Player return_enemy = new_position.enemy;

    new_position.removePiece(new_position.enemy, attacked_piece, bitboard::bitAtIndex(attacked_square));
    Player us = new_position.us;

    Score score[2] = {0, 0};
    Piece attacker_stage[2] = {pawn, pawn};

    while (true)
    {
        switch (attacker_stage[us])
        {
            case pawn:
            {
                uint64_t attack_mask =
                    bitboard::attackMaskPawnCapture(switchPlayer(us), attacked_square) & new_position.players[us] & new_position.pieces[pawn];
                if (attack_mask != 0)
                {
                    new_position.removePiece(us, pawn, bitboard::bitAtIndex(bitboard::ctz(attack_mask)));
                    score[us] += score_list[attacked_piece];
                    attacked_piece = pawn;
                }
                else
                {
                    attacker_stage[us] += 1;
                    continue;
                }
                break;
            }
            case king:
            {
                return score[return_us] - score[return_enemy];
            }
            default:
            {
                uint64_t attack_mask =
                    bitboard::attackMask(attacker_stage[us], attacked_square, new_position.occupancy()) & new_position.players[us] &
                    new_position.pieces[attacker_stage[us]];
                if (attack_mask != 0)
                {
                    new_position.removePiece(us, attacker_stage[us], bitboard::bitAtIndex(bitboard::ctz(attack_mask)));
                    score[us] += score_list[attacked_piece];
                    attacked_piece = attacker_stage[us];
                }
                else
                {
                    attacker_stage[us] += 1;
                    continue;
                }
                break;
            }
        }

        if (score[us] - score_list[attacked_piece] - score[switchPlayer(us)] >= 0)
        {
            return score[return_us] - score[return_enemy];
        }

        us = switchPlayer(us);
    }
}

Score Position::see(const Move move) const
{
    Position new_position = *this;

    if ((score_list[move.captured()] + score_list[move.promoted()]) < score_list[move.moved()])
    {
        new_position.doMove(move);
        Score seeScore = score_list[move.captured()] + score_list[move.promoted()] - new_position.see(move.to(), move.moved());
        if (seeScore < 0)
        {
            return seeScore;
        }
    }
    return move.mvvLva();
}

uint64_t Position::getZobristKey() const
{
    uint64_t ret = 0;
    for (Piece piece = 0; piece < no_piece; piece += 1)
    {
        if (pieces[piece] != 0)
        {
            uint64_t tmp_occupancy = pieces[piece];
            while (tmp_occupancy != 0)
            {
                Square squareIndex = bitboard::rto(tmp_occupancy);
                if ((bitboard::bitAtIndex(squareIndex) & players[white]) != 0)
                {
                    ret ^= bitboard::zobristMaskPlayers(white, squareIndex);
                }
                else
                {
                    ret ^= bitboard::zobristMaskPlayers(black, squareIndex);
                }
                ret ^= bitboard::zobristMaskPieces(piece, squareIndex);
            }
        }
    }
    ret ^= enpassant_castling;
    ret ^= bitboard::zobristMaskSideToMove(us);
    return ret;
}

uint8_t Position::generateCaptures(Move* moves) const
{
    uint8_t i = 0; // i := number of moves
    i += movegen::generatePawnCaptures(&moves[i], *this);
    i += movegen::generateCaptures(knight, &moves[i], *this);
    i += movegen::generateCaptures(bishop, &moves[i], *this);
    i += movegen::generateCaptures(rook, &moves[i], *this);
    i += movegen::generateCaptures(queen, &moves[i], *this);
    i += movegen::generateCaptures(king, &moves[i], *this);
    infoassert(i <= max_num_captures);
    return i;
}

uint8_t Position::generateQuiets(Move* moves) const
{
    uint8_t i = 0; // i := number of moves
    i += movegen::generatePawnQuiets(&moves[i], *this);
    i += movegen::generateCastlingMoves(&moves[i], *this);
    i += movegen::generateQuiets(knight, &moves[i], *this);
    i += movegen::generateQuiets(bishop, &moves[i], *this);
    i += movegen::generateQuiets(rook, &moves[i], *this);
    i += movegen::generateQuiets(queen, &moves[i], *this);
    i += movegen::generateQuiets(king, &moves[i], *this);
    infoassert(i <= max_num_quiets);
    return i;
}

uint8_t Position::generateMoves(Move* moves) const
{
    uint8_t num_captures = this->generateCaptures(&moves[0]);
    uint8_t num_quiets = this->generateQuiets(&moves[num_captures]);
    return num_captures + num_quiets;
}

void Position::doMove(const Move move)
{
    const Square to = move.to();
    const Square from = move.from();
    const Piece moved = move.moved();
    const Piece captured = move.captured();
    const Piece promoted = move.promoted();
    const Square enpassant = move.enpassant();

    this->zobrist_key ^= this->enpassant_castling;

    this->enpassant_castling &= (bitboard::rank(a1) | bitboard::rank(a8));
    this->enpassant_castling &= ~bitboard::bitAtIndex(from);
    this->enpassant_castling &= ~bitboard::bitAtIndex(to);
    if (enpassant != no_square)
    {
        this->enpassant_castling |= bitboard::bitAtIndex(enpassant);
    }
    this->zobrist_key ^= this->enpassant_castling;
    this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, from);
    this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, to);
    this->zobrist_key ^= bitboard::zobristMaskPieces(moved, from);
    this->zobrist_key ^= bitboard::zobristMaskPieces(moved, to);
    // en passant
    if (move.captured_enpassant())
    {
        this->removePiece(this->enemy, pawn, bitboard::attackMaskPawnQuiet(this->enemy, to));
        this->movePiece(this->us, pawn, bitboard::bitAtIndex(from), bitboard::bitAtIndex(to));

        const Square capturedIndex = bitboard::ctz(bitboard::attackMaskPawnQuiet(this->enemy, to));
        this->zobrist_key ^= bitboard::zobristMaskPieces(pawn, capturedIndex);
        this->zobrist_key ^= bitboard::zobristMaskPlayers(this->enemy, capturedIndex);
    }
        // castling
    else if (move.castled())
    {
        // queenside
        if (to == castling::queenside_king_to_square[this->us])
        {
            this->movePiece(this->us, king, castling::king_from[this->us], castling::queenside_king_to[this->us]);
            this->movePiece(this->us, rook, castling::queenside_rook_from[this->us], castling::queenside_rook_to[this->us]);

            this->zobrist_key ^= bitboard::zobristMaskPieces(rook, castling::queenside_rook_from_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPieces(rook, castling::queenside_rook_to_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, castling::queenside_rook_from_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, castling::queenside_rook_to_square[this->us]);
        }
            // kingside
        else
        {
            this->movePiece(this->us, king, castling::king_from[this->us], castling::kingside_king_to[this->us]);
            this->movePiece(this->us, rook, castling::kingside_rook_from[this->us], castling::kingside_rook_to[this->us]);

            this->zobrist_key ^= bitboard::zobristMaskPieces(rook, castling::kingside_rook_from_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPieces(rook, castling::kingside_rook_to_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, castling::kingside_rook_from_square[this->us]);
            this->zobrist_key ^= bitboard::zobristMaskPlayers(this->us, castling::kingside_rook_to_square[this->us]);
        }
    }
    else
    {
        if (captured != no_piece)
        {
            this->removePiece(this->enemy, captured, bitboard::bitAtIndex(to));

            this->zobrist_key ^= bitboard::zobristMaskPieces(captured, to);
            this->zobrist_key ^= bitboard::zobristMaskPlayers(this->enemy, to);
        }
        if (promoted != no_piece)
        {
            this->removePiece(this->us, moved, bitboard::bitAtIndex(from));
            this->addPiece(this->us, promoted, bitboard::bitAtIndex(to));

            this->zobrist_key ^= bitboard::zobristMaskPieces(moved, to);
            this->zobrist_key ^= bitboard::zobristMaskPieces(promoted, to);
        }
        else
        {
            this->movePiece(this->us, moved, bitboard::bitAtIndex(from), bitboard::bitAtIndex(to));
        }
    }
    if (moved == pawn || captured != no_piece)
    {
        this->halfmove_clock = 0;
    }
    else
    {
        this->halfmove_clock += 1;
    }
    if (this->us == black)
    {
        this->fullmoves_played += 1;
    }
    this->enemy = switchPlayer(this->enemy);
    this->us = switchPlayer(this->us);
    this->zobrist_key ^= bitboard::zobristMaskSideToMove(white);
    this->zobrist_key ^= bitboard::zobristMaskSideToMove(black);
}

void Position::doNullMove()
{
    this->zobrist_key ^= this->enpassant_castling;
    this->enpassant_castling &= (bitboard::rank(a1) | bitboard::rank(a8));
    this->zobrist_key ^= this->enpassant_castling;
    this->zobrist_key ^= bitboard::zobristMaskSideToMove(white);
    this->zobrist_key ^= bitboard::zobristMaskSideToMove(black);

    this->enemy = switchPlayer(this->enemy);
    this->us = switchPlayer(this->us);
}
