![alt text](https://gitlab.com/tsoj/Googleplex_Starthinker/raw/master/Googleplex_Starthinker.png)
UCI chess engine

You can play against it [here](https://lichess.org/@/squared-chess).

##### Download:
```
git clone https://gitlab.com/tsoj/Googleplex_Starthinker.git
```
##### Compile
```
cd Googleplex_Starthinker/
cmake .
make
```
##### Run:
```
./Googleplex_Starthinker
```

##### Features

- implementation:
  - [x] kindergarten bitboards
  - [x] staged move generation
- evaluation:
  - [x] piece square tables
  - [x] isolated pawns
  - [x] passed pawns
  - [x] mobility
  - [x] outposts
  - [x] king unsafety
- search:
  - [x] alpha-beta/negamax
  - [x] principle variation search
  - [x] quiescence search
  - [x] transposition table
  - [x] iterativ deepening
  - move ordering:
    - [x] transposition table
    - [x] MVV-LVA
    - [x] static exchange evaluation
    - [x] killermoves
    - [x] history heuristic
  - [x] nullmove pruning
  - [x] late move reductions
  - [x] check extensions
  - [x] delta pruning
  - [x] futility pruning

##### Trivia

*"...the Googleplex Starthinker in the Seventh Galaxy of Light and Ingenuity which can calculate the trajectory of every single dust particle throughout a five-week Aldebaran sand blizzard..."*

[Wikipedia](https://en.wikipedia.org/wiki/List_of_minor_The_Hitchhiker%27s_Guide_to_the_Galaxy_characters#Googleplex_Starthinker)

##### License

Copyright (c) 2019 Jost Triller
